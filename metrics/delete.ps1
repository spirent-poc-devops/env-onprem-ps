#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "metrics",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)

#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

###################################################################
try {
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if ((Test-EnvMapValue -Map $resources -Key "$ResourcePrefix") -and (Test-EnvMapValue -Map $resources -Key "$ResourcePrefix.port"))
    {
        # Check k8s yml file exists
        if (!(Test-Path -Path "$($path)/../temp/metrics.yml")) {
            Write-Error "Missing metrics' yml file in the 'temp' folder. Try recreating the component to generate the yml file."
        }

        # Notify user about start of the task
        Write-Host "`n***** Started deleting metrics component. *****`n"
        Log-Info "Started deleting metrics component"

        # Switch to k8s cluster from config
        . "$path/../switch_env.ps1" -ConfigPath $ConfigPath
        $path = $PSScriptRoot

        kubectl delete -f "$($path)/../temp/metrics.yml"

        if ($LastExitCode -ne 0){
            Write-Error "There were errors deleting metrics, Watch logs above"
            Log-Error "There were errors deleting metrics, Watch logs above"
        }

        # Delete results and save resource file to disk
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.port"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.host"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint"
        #TODO: remove other resources
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

        # Notify user about end of the task
        $status = "DELETION_COMPLETED"
        Write-Host "`n***** Completed deleting metrics component. *****`n"
        Log-Info "Completed deleting metrics component."
    }
    else 
    {
        $status = "DELETION_SKIPPED"
        exit 0
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "Metrics $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    if ($status -eq "DELETION_FAILED"){
        Log-Error "Metrics $status in $totalTime (HH:mm:ss.ms)" 
    }else{
        Log-Info "Metrics $status in $totalTime (HH:mm:ss.ms)" 
    }
}

###################################################################
