#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "metrics",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix,

    [Parameter(Mandatory=$false, Position=4)]
    [string] $KubernetesPrefix = "k8s"
)

#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

###################################################################
try {
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    # Skip if already installed
    if (!((Test-EnvMapValue -Map $resources -Key "$ResourcePrefix") -and (Test-EnvMapValue -Map $resources -Key "$ResourcePrefix.port"))) {
        # Verify that k8s cluster created
        if ((Test-EnvMapValue -Map $resources -Key "$KubernetesPrefix") -and (Test-EnvMapValue -Map $resources -Key "$KubernetesPrefix.type"))
        {
            # Notify user about start of the task
            Write-Host "`n***** Started creating metrics components. *****`n"

            # Switch to k8s cluster from config
            . "$path/../switch_env.ps1" -ConfigPath $ConfigPath
            $path = $PSScriptRoot

            $namespace = Get-EnvMapValue -Map $config -Key "$KubernetesPrefix.namespace"

            $templateParams = @{ namespace=$namespace ; }
            # Set variables from config
            Build-EnvTemplate -InputPath "$($path)/templates/metrics.yml" -OutputPath "$($path)/../temp/metrics.yml" -Params1 $templateParams
            # Create k8s component
            kubectl apply -f "$($path)/../temp/metrics.yml"

            # Notify user about end of the task
            Write-Host "`n***** Completed creating metrics components. *****`n"
            Log-Info "Completed creating metrics components"


            # Record results and save them to disk
            $port = kubectl get svc grafana -n $namespace -o=jsonpath="{.spec.ports[0].targetPort}"
            $serviceHost = "grafana.$namespace.svc.cluster.local"
            $endpoint = "$($serviceHost):$($port)"
            Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.port" -Value $port
            Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.host" -Value $serviceHost
            Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint" -Value $endpoint
            Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
            $status = "CREATION_COMPLETED"
        }
        else
        {
            Write-Error "Missing kuberentes cluster. Please install kubernetes first."
            Log-Error "Missing kuberentes cluster. Please install kubernetes first."
        }
    } else
    {
        $status = "CREATION_SKIPPED"
        Write-Host "metrics component already installed. Installation skipped."
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "Metrics $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    if ($status -eq "CREATION_FAILED"){
        Log-Error "Metrics $status in $totalTime (HH:mm:ss.ms)" 
    }else{
        Log-Info "Metrics $status in $totalTime (HH:mm:ss.ms)" 
    }
}

###################################################################
