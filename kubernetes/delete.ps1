#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "k8s",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

#Set Log Component 
$env:LogComponent=$ConfigPrefix

###################################################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$ResourcePrefix.type")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting k8s cluster. *****`n"
        Log-Info "Started deleting k8s cluster"

        # Prepare hosts file if missing
        if (!(Test-Path -Path "$path/../temp/k8s_ansible_hosts")) {
            $ansibleInventory = @("[master_init]")
            $ansibleInventory += "master_init ansible_host=$($(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips")[0]) ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
            $ansibleInventory += "`r`n[masters_redundant]"
            $i = 0
            foreach ($node in $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips" | select -skip 1)) {
                $ansibleInventory += "master_redundant$i ansible_host=$node ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
                $i++
            }
            $ansibleInventory += "`r`n[workers]"
            $i = 0
            foreach ($node in (Get-EnvMapValue -Map $config -Key "$ConfigPrefix.worker_ips")) {
                $ansibleInventory += "worker$i ansible_host=$node ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
                $i++
            }
            Set-Content -Path "$path/../temp/k8s_ansible_hosts" -Value $ansibleInventory
        }

        # Whitelist nodes
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        }

        # Destroy k8s cluster
        Build-EnvTemplate -InputPath "$($path)/templates/k8s_uninstall_playbook.yml" -OutputPath "$($path)/../temp/k8s_uninstall_playbook.yml" -Params1 $templateParams
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_uninstall_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_uninstall_playbook.yml"
        }

        # Remove k8s cluster from mgmt station .kube/config
        $envPrefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
        $k8sUsername = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username"
        Write-Host "Removing $envPrefix k8s cluster from mgmt station .kube/config..."
        Log-Info "Removing $envPrefix k8s cluster from mgmt station .kube/config"

        kubectl config unset "users.$envPrefix-k8s-admin"
        kubectl config unset "contexts.$envPrefix-k8s"
        kubectl config unset "clusters.$envPrefix-k8s"

        if (Test-Path "/home/$k8sUsername/.kube") {
            Remove-Item -Path "/home/$k8sUsername/.kube/config_$envPrefix" -Force
        } 
        
        # Delete results and save resource file to disk
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.type"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.inventory"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.blobs_storage_gb"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.namespace"

        # Remove component resources runned in k8s cluster (they will not be accesible after k8s cluster deleted)
        # Remove-EnvMapValue -Map $resources -Key "kafka.port"
        # Remove-EnvMapValue -Map $resources -Key "redis.port"
        # Remove-EnvMapValue -Map $resources -Key "consul.state"
        # Remove-EnvMapValue -Map $resources -Key "connectparams.status"
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting k8s cluster. *****`n"
        Log-Info "Completed deleting k8s cluster"
    }
    else 
    {
        $status = "DELETION_SKIPPED"
        exit 0
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################
