#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "k8s",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

#Set Log Component 
$env:LogComponent=$ConfigPrefix

###################################################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    # Skip if already installed
    if (!(Get-EnvMapValue -Map $resources -Key "$ResourcePrefix.type")) {
        # Notify user about start of the task
        Write-Host "`n***** Started creating k8s cluster. *****`n"
        Log-Info "Started creating k8s cluster"

        # Prepare hosts file
        $ansibleInventory = @("[master_init]")

        if ($(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips").Count -gt 1) {
            # Initial master
            $ansibleInventory += "master_init ansible_host=$($(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips")[0]) ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
        
            # Redundant masters
            $ansibleInventory += "`r`n[masters_redundant]"
            $i = 0
            foreach ($node in $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips" | select -skip 1)) {
                $ansibleInventory += "master_redundant$i ansible_host=$node ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
                $i++
            }

            # Loadbalancer
            $ansibleInventory += "`r`n[lb]"
            if (Get-EnvMapValue -Map $config -Key "$ConfigPrefix.loadbalancer.haproxy") {
                $ansibleInventory += "lb0 ansible_host=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.loadbalancer.host") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
                }
            } else {
            # single master
            $ansibleInventory += "master_init ansible_host=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
        }
        
        $ansibleInventory += "`r`n[workers]"
        $i = 0
        foreach ($node in (Get-EnvMapValue -Map $config -Key "$ConfigPrefix.worker_ips")) {
            $ansibleInventory += "worker$i ansible_host=$node ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.private_key_path")"
            $i++
        }
        Set-Content -Path "$path/../temp/k8s_ansible_hosts" -Value $ansibleInventory

        # Whitelist nodes
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        }

        # Install k8s dependecies
        $k8s_cni_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.cni_version"
        $k8s_crictl_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.crictl_version"
        $k8s_kubelet_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.kubelet_version"
        $k8s_kubeadm_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.kubeadm_version"
        $k8s_kubectl_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.kubectl_version"
        $templateParams = @{ k8s_cni_version=$k8s_cni_version ; k8s_crictl_version=$k8s_crictl_version ; k8s_kubelet_version=$k8s_kubelet_version ; k8s_kubeadm_version=$k8s_kubeadm_version ; k8s_kubectl_version=$k8s_kubectl_version }
        Build-EnvTemplate -InputPath "$($path)/templates/k8s_prerequisites_playbook.yml" -OutputPath "$($path)/../temp/k8s_prerequisites_playbook.yml" -Params1 $templateParams
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_prerequisites_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_prerequisites_playbook.yml"
        }

        if ($(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips").Count -gt 1) {
            # Multimaster cluster

            # Configure loadbalancer
            if (Get-EnvMapValue -Map $config -Key "$ConfigPrefix.loadbalancer.haproxy") {
                $haproxy_masters_endpoints = ""
                foreach($masterIp in Get-EnvMapValue -Map $config -Key "$ConfigPrefix.master_ips") {
                    $haproxy_masters_endpoints += "                server master1 $($masterIp):6443 check`n"
                }
                $templateParams = @{ 
                    haproxy_masters_endpoints=$haproxy_masters_endpoints
                }
                Build-EnvTemplate -InputPath "$($path)/templates/k8s_lb_playbook.yml" -OutputPath "$($path)/../temp/k8s_lb_playbook.yml" -Params1 $templateParams
                if ($config["extended_ansible_logs"]) {
                    ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_lb_playbook.yml"
                } else {
                    ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_lb_playbook.yml"
                }
            }

            # Init k8s cluster on master node
            $env_prefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
            $k8s_network = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.network"
            $k8s_username = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username"
            $lb_host = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.loadbalancer.host"
            $lb_port = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.loadbalancer.port"
            $templateParams = @{ 
                env_prefix=$env_prefix;
                k8s_network=$k8s_network;
                k8s_username=$k8s_username;
                lb_host=$lb_host;
                lb_port=$lb_port;
            }
            Build-EnvTemplate -InputPath "$($path)/templates/k8s_master_init_playbook.yml" -OutputPath "$($path)/../temp/k8s_master_init_playbook.yml" -Params1 $templateParams
            if ($config["extended_ansible_logs"]) {
                ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_master_init_playbook.yml"
            } else {
                ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_master_init_playbook.yml"
            }
            # Initialize redundant masters
            $k8s_network = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.network"
            $k8s_username = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username"
            $templateParams = @{ k8s_network=$k8s_network ; k8s_username=$k8s_username }
            Build-EnvTemplate -InputPath "$($path)/templates/k8s_master_redundant_playbook.yml" -OutputPath "$($path)/../temp/k8s_master_redundant_playbook.yml" -Params1 $templateParams
            if ($config["extended_ansible_logs"]) {
                ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_master_redundant_playbook.yml"
            } else {
                ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_master_redundant_playbook.yml"
            }
        } else {
            # Single master cluster
            $env_prefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
            $k8s_network = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.network"
            $k8s_username = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username"
            $templateParams = @{ 
                env_prefix=$env_prefix;
                k8s_network=$k8s_network;
                k8s_username=$k8s_username;
            }
            Build-EnvTemplate -InputPath "$($path)/templates/k8s_master_single_playbook.yml" -OutputPath "$($path)/../temp/k8s_master_single_playbook.yml" -Params1 $templateParams
            if ($config["extended_ansible_logs"]) {
                ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_master_single_playbook.yml"
            } else {
                ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_master_single_playbook.yml"
            }
        }

        # Join workers to master
        Build-EnvTemplate -InputPath "$($path)/templates/k8s_worker_playbook.yml" -OutputPath "$($path)/../temp/k8s_worker_playbook.yml" -Params1 $templateParams
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_worker_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/k8s_ansible_hosts" "$path/../temp/k8s_worker_playbook.yml"
        }

        if ($LastExitCode -ne 0) {
            Write-Error "Error on installing k8s cluster. See logs above"
            Log-Error "Error on installing k8s cluster. See logs above"
        }

        # Replace k8s cluster name in new kube config
        (Get-Content -Path "/home/$k8s_username/.kube/config_$env_prefix" -Raw)  -replace "kubernetes-admin@kubernetes","$env_prefix-k8s" -replace "kubernetes","$env_prefix-k8s" | Set-Content "/home/$k8s_username/.kube/config_$env_prefix"

        if (!(Test-Path -Path "/home/$k8s_username/.kube/config") -or [String]::IsNullOrWhiteSpace((Get-content "/home/$k8s_username/.kube/config"))) {
            # First k8s cluster created from mgmt station. Just copy config
            Get-Content -Path "/home/$k8s_username/.kube/config_$env_prefix" | Add-Content -Path "/home/$k8s_username/.kube/config"
        } else {
            # Add new cluster to mgmt station kube/config
            $mgmtConfigText = Get-Content "/home/$k8s_username/.kube/config"
            $newEnvConfigText = Get-Content "/home/$k8s_username/.kube/config_$env_prefix"

            $newMgmtConfigText = @() 

            # indexes are default for kubeadm configs
            $newCluserSection = $newEnvConfigText[2..5]
            $newContextSection = $newEnvConfigText[7..10]
            $newUserSection = $newEnvConfigText[15..18]

            foreach ($Line in $mgmtConfigText) {    
                $newMgmtConfigText += $Line

                if ($Line -match "clusters:") {
                    $newMgmtConfigText += $newCluserSection
                } 
                if ($Line -match "contexts:") {
                    $newMgmtConfigText += $newContextSection
                } 
                if ($Line -match "users:") {
                    $newMgmtConfigText += $newUserSection
                } 
            }

            Set-Content "/home/$k8s_username/.kube/config" $newMgmtConfigText
        }

        # Write k8s resources
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.type" -Value "kubeadm"
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.inventory" -Value $ansibleInventory
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

        # Switch to new k8s cluster
        . "$path/../switch_env.ps1" -ConfigPath $ConfigPath
        $path = $PSScriptRoot

        # Notify user about end of the task
        Write-Host "`n***** Completed creating k8s cluster. *****`n"
        Log-Info "Completed creating k8s cluster"
        ###################################################################


        ###################################################################
        # Notify user about start of the task
        Write-Host "`n***** Started creating k8s namespace and blobs persistent volume. *****`n"
        Log-Info "Started creating k8s namespace and blobs persistent volume"

        # Set variables from config
        $k8s_namespace = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.namespace"
        $templateParams = @{ k8s_namespace=$k8s_namespace }
        Build-EnvTemplate -InputPath "$($path)/templates/namespace.yml" -OutputPath "$($path)/../temp/namespace.yml" -Params1 $templateParams
        # Create k8s namespace
        kubectl apply -f "$($path)/../temp/namespace.yml"

        # Set variables from config
        $k8s_namespace = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.namespace"
        $k8s_blobs_storage_gb = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.blobs_storage_gb"
        $templateParams = @{ k8s_namespace=$k8s_namespace ; k8s_blobs_storage_gb=$k8s_blobs_storage_gb }
        Build-EnvTemplate -InputPath "$($path)/templates/blobs_pv.yml" -OutputPath "$($path)/../temp/blobs_pv.yml" -Params1 $templateParams
        # Create k8s blobs persistent volume
        kubectl apply -f "$($path)/../temp/blobs_pv.yml"

        # Write k8s resources
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.namespace" -Value (Get-EnvMapValue -Map $config -Key "$ConfigPrefix.namespace")
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.blobs_storage_gb" -Value (Get-EnvMapValue -Map $config -Key "$ConfigPrefix.blobs_storage_gb")
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

        $status = "CREATION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed creating k8s namespace and blobs persistent volume. *****`n"
        Log-Info "Completed creating k8s namespace and blobs persistent volume"
    } else
    {
        $status = "CREATION_SKIPPED"
        Write-Host "Kubernetes component already installed. Installation skipped."
        Log-Info "Kubernetes component already installed. Installation skipped"
        exit 0
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################
