# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

function Test-KubernetesStatus
{
    $Report= @{}
    
    # Test the existance of kubernetes
    $Command = 'kubectl get nodes'
    $Report["Check if kubernetes is running"] = Match-CommandOutput $(Invoke-Expression $Command | Out-String) 'Ready'
    
    Print-TestComponent  "Kubernetes" $Report 
}

function Test-KafkaStatus
{
    $Report= @{}

    # Test the existance & status of the Kafka pod
    $Command = 'kubectl get pods kafka-0 -n infra -o jsonpath="Name: {.metadata.name} Status: {.status.phase}"'
    $Report["Check if kafka is running"] = Match-CommandOutput $(Invoke-Expression $Command | Out-String) 'Running'

    Print-TestComponent  "Kafka" $Report
}

function Test-RedisStatus
{
    $Report= @{}

    # Test the existance & status of the Redis pod
    $Command = 'kubectl get pods redis-0 -n infra -o jsonpath="Name: {.metadata.name} Status: {.status.phase}"'
    $Report["Check if Redis is running"] = Match-CommandOutput $(Invoke-Expression $Command | Out-String) 'Running'

    Print-TestComponent  "Redis" $Report
}

# function Test-TsdbStatus
# {
#     $Report= @{}

#     # Test the existance & status of the TSDB pod 
#     $Command = 'kubectl get pods -n infra'
#     $temp = $(Invoke-Expression $Command) -match "timescale-"
#     if ($temp)
#     {        
#         if ($temp -match "Running")
#         {
#             $Report["Check if the TSDB pod exists"] = "Success"
#         } else
#         {
#             $Report["Check if the TSDB pod exists"] = "Fail"
#         }
#     } else
#     {
#         $Report["Check if the TSDB pod exists"] = "Fail"
#     }

#     Print-TestComponent "Timescale DB" $Report 
# }


