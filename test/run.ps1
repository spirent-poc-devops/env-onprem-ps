#!/usr/bin/env pwsh

# Set parameters
param(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath
)

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq ""))
{
    $ResourcePrefix = $ConfigPrefix
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath
$initialPath = Get-Location



$ErrorActionPreference = "Stop"

Write-Host $Environment
# Load test functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/tests/test-statuses.ps1"

#Minikube Test
Test-KubernetesStatus
#Kafka test 
Test-KafkaStatus
#Redis
Test-RedisStatus
#Timescale
# Test-TsdbStatus


