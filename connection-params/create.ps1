#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "connectparams",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix,

    [Parameter(Mandatory=$false, Position=4)]
    [string] $KubernetesPrefix = "k8s",

    [Parameter(Mandatory=$false, Position=6)]
    [string] $RedisPrefix = "redis",

    [Parameter(Mandatory=$false, Position=7)]
    [string] $KafkaPrefix = "kafka",

    [Parameter(Mandatory=$false, Position=8)]
    [string] $TimescalePrefix = "timescale",

    [Parameter(Mandatory=$false, Position=9)]
    [string] $Neo4jPrefix = "neo4j"
)

#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

###################################################################
# Skip if already installed
if (!((Test-EnvMapValue -Map $resources -Key "$ResourcePrefix") -and (Test-EnvMapValue -Map $resources -Key "$ResourcePrefix.status"))) {
    # Verify that k8s cluster created
    if ((Test-EnvMapValue -Map $resources -Key "$KubernetesPrefix") -and (Test-EnvMapValue -Map $resources -Key "$KubernetesPrefix.type"))
    {
        # Notify user about start of the task
        Write-Host "`n***** Started creating connection config map. *****`n"
        Log-Info "Started creating connection config map."

        # Switch to k8s cluster from config
        . "$path/../switch_env.ps1" -ConfigPath $ConfigPath
        $path = $PSScriptRoot
        
        $namespace = Get-EnvMapValue -Map $config -Key "$KubernetesPrefix.namespace"
        $k8s_namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
        $k8s_master_ip = Get-EnvMapValue -Map $config -Key "k8s.master_ip"
        $redis_port = Get-EnvMapValue -Map $resources -Key "redis.port"
        $kafka_port = Get-EnvMapValue -Map $resources -Key "kafka.port"

        $timescale_ip = Get-EnvMapValue -Map $resources -Key "$TimescalePrefix.host"
        $timescale_port = Get-EnvMapValue -Map $resources -Key "$TimescalePrefix.port"
        $timescale_name = Get-EnvMapValue -Map $config -Key "$TimescalePrefix.name"
        $timescale_user = Get-EnvMapValue -Map $config -Key "$TimescalePrefix.username"
        $timescale_pass = Get-EnvMapValue -Map $config -Key "$TimescalePrefix.password"


        $neo4j_ip = Get-EnvMapValue -Map $resources -Key "$Neo4jPrefix.host"
        $neo4j_port = Get-EnvMapValue -Map $resources -Key "$Neo4jPrefix.port"
        $neo4j_bolt_port = Get-EnvMapValue -Map $resources -Key "$Neo4jPrefix.bolt_port"
        $neo4j_pass = Get-EnvMapValue -Map $config -Key "$Neo4jPrefix.password"

        $templateParams = @{ 
            namespace=$namespace;
            k8s_namespace=$k8s_namespace;
            k8s_master_ip=$k8s_master_ip;
            redis_port=$redis_port; 
            kafka_port=$kafka_port; 
            timescale_ip=$timescale_ip; 
            timescale_port=$timescale_port; 
            timescale_name=$timescale_name; 
            timescale_user=$timescale_user; 
            timescale_pass=$timescale_pass 
            neo4j_ip=$neo4j_ip; 
            neo4j_port=$neo4j_port; 
            neo4j_bolt_port=$neo4j_bolt_port; 
            neo4j_pass=$neo4j_pass; 
        }
        Build-EnvTemplate -InputPath "$($path)/templates/connection-params.yml" -OutputPath "$($path)/../temp/connection-params.yml" -Params1 $templateParams
    
        # Create connection config map
        kubectl apply -f "$($path)/../temp/connection-params.yml"

        # Record results and save them to disk
        # Todo: read actual state
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.status" -Value "created"
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

        # Notify user about end of the task
        Write-Host "`n***** Completed creating connection config map. *****`n"
        Log-Info "Completed creating connection config map."
        ###################################################################
    } else
    {
        Write-Error "Missing kuberentes cluster. Please install kubernetes first."
        Log-Error "Missing kuberentes cluster. Please install kubernetes first."
    }
} else
{
    Write-Host "Connection params component already installed. Installation skipped."
    Log-Warn "Connection params component already installed. Installation skipped."
}
###################################################################
