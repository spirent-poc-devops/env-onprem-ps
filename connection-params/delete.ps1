#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "connectparams",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)


#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

###################################################################
# Skip if resource wasn't created
if ((Test-EnvMapValue -Map $resources -Key "$ResourcePrefix") -and (Test-EnvMapValue -Map $resources -Key "$ResourcePrefix.status"))
{
    # Notify user about start of the task
    Write-Host "`n***** Started deleting connection config map. *****`n"
    Log-Info "Started deleting connection config map."

    # Switch to k8s cluster from config
    . "$path/../switch_env.ps1" -ConfigPath $ConfigPath
    $path = $PSScriptRoot
    
    kubectl delete -f "$($path)/../temp/connection-params.yml"

    # Delete results and save resource file to disk
    Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.status"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

    # Notify user about end of the task
    Write-Host "`n***** Completed deleting connection config map. *****`n"
    Log-Info "Completed deleting connection config map."
}
else 
{
    exit 0
}
###################################################################
