#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "connectparams",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath


###################################################################
# Skip if resource wasn't created
if (Get-EnvMapValue -Map $resources -Key "$ResourcePrefix.status")
{
    # Notify user about start of the task
    Write-Host "`n***** Started updating connection config map. *****`n"

    # Switch to k8s cluster from config
    . "$path/../switch_env.ps1" -ConfigPath $ConfigPath
    $path = $PSScriptRoot

    # Set variables from config
    $k8s_namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
    $k8s_master_ip = Get-EnvMapValue -Map $config -Key "k8s.master_ip"
    $redis_port = Get-EnvMapValue -Map $resources -Key "redis.port"
    $kafka_port = Get-EnvMapValue -Map $resources -Key "kafka.port"
    $timescale_ip = Get-EnvMapValue -Map $config -Key "timescale.ip"
    $timescale_port = Get-EnvMapValue -Map $config -Key "timescale.port"
    $timescale_name = Get-EnvMapValue -Map $config -Key "timescale.name"
    $timescale_username = Get-EnvMapValue -Map $config -Key "timescale.username"
    $timescale_password = Get-EnvMapValue -Map $config -Key "timescale.password"
    $templateParams = @{ 
        k8s_namespace=$k8s_namespace
        k8s_master_ip=$k8s_master_ip
        redis_port=$redis_port
        kafka_port=$kafka_port
        timescale_ip=$timescale_ip
        timescale_port=$timescale_port
        timescale_name=$timescale_name
        imescale_username=$imescale_username
        timescale_password=$timescale_password 
    }
    Build-EnvTemplate -InputPath "$($path)/templates/connection-params.yml" -OutputPath "$($path)/../temp/connection-params.yml" -Params1 $templateParams
    # Update connection config map component
    kubectl apply -f "$($path)/../temp/connection-params.yml"

    # Notify user about end of the task
    Write-Host "`n***** Completed updating connection config map. *****`n"

    # Record results and save them to disk
    # Todo: read actual state
    Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.status" -Value "created"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
else
{
    Write-Error "Cann't execute update script - connection config map must be created before running update script."
}
###################################################################
