#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=1)]
    [string] $ResourcePath
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}

#Create LogFile
Create-LogFile

try{
    $createStartTime = $(get-date)
    # statusMap is a global map which stores create status for each component. Each component adds its status
    # at end of each script
    $statusMap = @{}

    # Clear errors variable for clean logging
    $error.clear()

    # Record environment information
    $env:LogComponent="environment"
    . "$($rootPath)/environment/create.ps1" -ConfigPath $ConfigPath -ConfigPrefix "environment" -ResourcePath $ResourcePath -ResourcePrefix "environment"
    ###################################################################
    # Create the k8s cluster
    try{
        $env:LogComponent="k8s"
        . "$($rootPath)/kubernetes/create.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "k8s" -ResourcePath "$ResourcePath" -ResourcePrefix "k8s"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create the k8s cluster. See logs above."
        Log-Error "Can't create the k8s cluster. See logs above."
    }
    ###################################################################

    ###################################################################
    try {
        $env:LogComponent="logging"
        # Create logging components
        . "$($rootPath)/logging/create.ps1" -ConfigPath $ConfigPath -ConfigPrefix "logging" -ResourcePath $ResourcePath -ResourcePrefix "logging"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create logging components. Watch logs above."
        Log-Error "Can't create logging components. Watch logs above."
    }

    ###################################################################
    try {
        $env:LogComponent="metrics"
        # Create metrics components
        . "$($rootPath)/metrics/create.ps1" -ConfigPath $ConfigPath -ConfigPrefix "metrics" -ResourcePath $ResourcePath -ResourcePrefix "metrics"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create metrics components. Watch logs above."
        Log-Error "Can't create metrics components. Watch logs above."
    }

    ###################################################################

    ###################################################################
    # Create the redis component
    try{
        $env:LogComponent="redis"
        . "$($rootPath)/redis/create.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "redis" -ResourcePath "$ResourcePath" -ResourcePrefix "redis"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create the redis component. See logs above."
        Log-Error "Can't create the redis component. See logs above."
    }

    ###################################################################

    ###################################################################
    # Create the kafka broker
    try{
        $env:LogComponent="kafka"
        . "$($rootPath)/kafka/create.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "kafka" -ResourcePath "$ResourcePath" -ResourcePrefix "kafka"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create kafka component. See logs above."
        Log-Error "Can't create kafka component. See logs above."
        
    }
    ###################################################################

    ###################################################################
    # Create the timescale db
    try{
        $env:LogComponent="timescale"
        . "$($rootPath)/timescale/create.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "timescale" -ResourcePath "$ResourcePath" -ResourcePrefix "timescale"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create timescale database. See logs above."
        Log-Error "Can't create timescale database. See logs above."
    }
    ###################################################################

    ###################################################################
    # Create the neo4j db
    try{
        $env:LogComponent="neo4j"
        . "$($rootPath)/neo4j/create.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "neo4j" -ResourcePath "$ResourcePath" -ResourcePrefix "neo4j"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create neo4j database. See logs above."
        Log-Error "Can't create neo4j database. See logs above."
    }
    ###################################################################

    ###################################################################
    # Create the connection config map
    try{
        $env:LogComponent="connectparams"
        . "$($rootPath)/connection-params/create.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "connectparams" -ResourcePath "$ResourcePath" -ResourcePrefix "connectparams"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create connection config map. See logs above."
        Log-Error "Can't create connection config map. See logs above."
    }
    ###################################################################

    ###################################################################
    # Try to lock config & resource file versions (by backing up to "./config/archive")
    try {
        $env:LogComponent="env-version-control"
        . "$($rootPath)/environment/version-control/create.ps1" -ConfigPath $ConfigPath -ConfigPrefix "environment" -ResourcePath $ResourcePath -ResourcePrefix "environment"
    } 
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't lock config & resource file versions. See logs above."
        Log-Error "Can't lock config & resource file versions. See logs above."
    }
}
finally {
    $elapsedTime = $(get-date) - $createStartTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    Write-Host "Summary for this run: " -ForegroundColor Yellow
    $statusMap | Format-Table @{ Label = "Component"; Expression={$_.Name}}, @{n='Summary';e={
        if ($_.Value -is [Hashtable]) {
            $ht = $_.Value
            $a  = $ht.keys | sort | ForEach-Object { '{0}={1}' -f $_, $ht[$_] }
            '{{{0}}}' -f ($a -join ', ')
        } else {
            $_.Value
        }
    }}
    Write-Host "Total time taken: $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
 }