#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "kafka",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix,

    [Parameter(Mandatory=$false, Position=4)]
    [string] $KubernetesPrefix = "k8s"
)

#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath


###################################################################
try{
    $status = "UPDATE_STARTED"
    $startTime = $(get-date)
    # Skip if already installed
    # if (!(Get-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint")) {
    if ((Get-EnvMapValue -Map $config -Key "$ConfigPrefix.update") -eq "true"){
        # Notify user about start of the task
        Write-Host "`n***** Started updating kafka cluster. *****`n"
        Log-Info "Started updating kafka cluster"

        # Verify for odd number of kafka nodes
        $kafkaVMCount = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.nodes_count"
        if ($kafkaVMCount % 2 -eq 0) {
            Write-Error "Kafka nodes count must be an odd number."
        }

        # Prepare hosts file
        $ansible_inventory = @("[kafka]")
        for ($i = 0; $i -lt $kafkaVMCount; $i++) {
            $ansible_inventory += "$i ansible_host=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_private_key_path")"
        }
        Set-Content -Path "$path/../temp/kafka_ansible_hosts" -Value $ansible_inventory

        # Whitelist nodes
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/kafka_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/kafka_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        }

        # Install kafka cluster
        $instance_username = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_username"
        $zookeeper_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.zookeeper_version"
        $kafka_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.kafka_version"
        $zookeeper_nodes = ""
        $kafka_nodes = ""
        for ($i = 0; $i -lt $kafkaVMCount; $i++) {
            $zookeeper_nodes += "`n        server.$i=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip"):2888:3888"
            $kafka_nodes += "$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip"):2181,"
        }

        $kafka_nodes = $kafka_nodes.Substring(0, $kafka_nodes.Length - 1)
        
        $templateParams = @{
            instance_username = $instance_username
            zookeeper_nodes=$zookeeper_nodes
            kafka_nodes=$kafka_nodes
            zookeeper_version=$zookeeper_version
            kafka_version=$kafka_version
        }
        
        Build-EnvTemplate -InputPath "$($path)/templates/kafka_install_playbook.yml" -OutputPath "$($path)/../temp/kafka_install_playbook.yml" -Params1 $templateParams
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/kafka_ansible_hosts" "$path/../temp/kafka_install_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/kafka_ansible_hosts" "$path/../temp/kafka_install_playbook.yml"
        }

        # Write kafka resources
        $port=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_port")
        $ip=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_ip")
        $endpoint = "$($ip):$($port)"
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.host" -Value $ip
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.port" -Value $port
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint" -Value $endpoint
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.inventory" -Value $ansible_inventory
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
        $status = "UPDATE_COMPLETED"

        Set-EnvMapValue -Map $config -Key "$ConfigPrefix.update" -Value false
        Write-EnvResources -ResourcePath $ConfigPath -Resources $config
        
        # Notify user about end of the task
        Write-Host "`n***** Completed updating kafka cluster. *****`n"
        Log-Info "Completed updating kafka cluster"
        }   
        else
        {
        $status = "UPDATE_SKIPPED"
        Write-Host "Update flag not set. So, skipping update for $ConfigPrefix" -ForegroundColor Yellow
        Log-Error "Update flag not set. So, skipping update for $ConfigPrefix"
        }   
    }
finally {
    if ($status -eq "UPDATE_STARTED"){
        $status = "UPDATE_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################