#!/usr/bin/env pwsh

param
(
    [Alias("hwc", "HWConfig")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $HWConfigPath,

    [Alias("hwr", "HWResources")]
    [Parameter(Mandatory=$false, Position=1)]
    [string] $HWResourcePath,

    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=2)]
    [string] $ConfigPath,

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePath
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($HWResourcePath -eq $null) -or ($HWResourcePath -eq ""))
{
    $HWResourcePath = ConvertTo-EnvResourcePath -ConfigPath $HWConfigPath
}
#Create LogFile
Create-LogFile

try{
    $createStartTime = $(get-date)
    $statusMap = @{}
    # Clear errors variable for clean logging
    $error.clear()
    ###################################################################
    # Create the virtual machines to simulate real hardware
    try{
        $env:LogComponent="hw"
        . "$($rootPath)/hardware/create.ps1" -HWConfigPath "$HWConfigPath" -HWConfigPrefix "hw" -HWResourcePath "$HWResourcePath" -HWResourcePrefix "hw" -ConfigPath "$ConfigPath"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't create hardware. See logs above."
        Log-Error "Can't create hardware. See logs above."
    }
    ###################################################################
}
finally {
    $elapsedTime = $(get-date) - $createStartTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    Write-Host "Summary for this run: " -ForegroundColor Yellow
    $statusMap | Format-Table @{ Label = "Component"; Expression={$_.Name}}, @{n='Summary';e={
        if ($_.Value -is [Hashtable]) {
            $ht = $_.Value
            $a  = $ht.keys | sort | ForEach-Object { '{0}={1}' -f $_, $ht[$_] }
            '{{{0}}}' -f ($a -join ', ')
        } else {
            $_.Value
        }
    }}
    Write-Host "Total time taken: $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
 }
