#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=1)]
    [string] $ResourcePath
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}

#Get-LogFile
Get-LogFile

try{
    $deleteStartTime = $(get-date)
    # statusMap is a global map which stores delete status for each component. Each component adds its status
    # at end of each script
    $statusMap = @{}
    # Clear errors variable for clean logging
    $error.clear()

    ###################################################################
    # Record environment information
    . "$($rootPath)/environment/delete.ps1" -ConfigPath $ConfigPath -ConfigPrefix "environment" -ResourcePath $ResourcePath -ResourcePrefix "environment"
    ###################################################################


    ###################################################################
    # Delete the redis component
    try{
        $env:LogComponent="redis"
        . "$($rootPath)/redis/delete.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "redis" -ResourcePath "$ResourcePath" -ResourcePrefix "redis"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete the redis component. See logs above."
    }
    ###################################################################


    ###################################################################
    # Delete the kafka component
    try{
        $env:LogComponent="kafka"
        . "$($rootPath)/kafka/delete.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "kafka" -ResourcePath "$ResourcePath" -ResourcePrefix "kafka"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete the kafka component. See logs above."
    }
    ###################################################################

    ###################################################################
    try {
        $env:LogComponent="metrics"
        # Destroy metrics components
        . "$($rootPath)/metrics/delete.ps1" -ConfigPath $ConfigPath -ConfigPrefix "metrics" -ResourcePath $ResourcePath -ResourcePrefix "metrics"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete metrics components. Watch logs above."
        Log-Error "Can't delete metrics components. Watch logs above."
    }


    ###################################################################
    try {
        $env:LogComponent="logging"
        # Destroy logging components
        . "$($rootPath)/logging/delete.ps1" -ConfigPath $ConfigPath -ConfigPrefix "logging" -ResourcePath $ResourcePath -ResourcePrefix "logging"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete logging components. Watch logs above."
        Log-Error "Can't delete logging components. Watch logs above."
    }

    ###################################################################

    ###################################################################
    # Delete the connection configmap
    try{
        $env:LogComponent="connectparams"
        . "$($rootPath)/connection-params/delete.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "connectparams" -ResourcePath "$ResourcePath" -ResourcePrefix "connectparams"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete connection config map. See logs above."
    }
    ###################################################################


    ###################################################################
    # Delete the k8s cluster
    try{
        $env:LogComponent="k8s"
        . "$($rootPath)/kubernetes/delete.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "k8s" -ResourcePath "$ResourcePath" -ResourcePrefix "k8s"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete the k8s cluster. See logs above."
    }
    ###################################################################

    ###################################################################
    # Delete the timescale db
    try{
        $env:LogComponent="timescale"
        . "$($rootPath)/timescale/delete.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "timescale" -ResourcePath "$ResourcePath" -ResourcePrefix "timescale"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete the timescale database. See logs above."
    }
    ###################################################################

    # Delete the Neo4j db
    try{
        $env:LogComponent="neo4j"
        . "$($rootPath)/neo4j/delete.ps1" -ConfigPath "$ConfigPath" -ConfigPrefix "neo4j" -ResourcePath "$ResourcePath" -ResourcePrefix "neo4j"
    }
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete the Neo4j database. See logs above."
    }

    ###################################################################
    # Try to delete (move to "archive/deleted") archived config & resource file versions
    try {
        $env:LogComponent="env-version-control"
        . "$($rootPath)/environment/version-control/delete.ps1" -ConfigPath $ConfigPath -ConfigPrefix "environment" -ResourcePath $ResourcePath -ResourcePrefix "environment"
    } 
    # Catch and log any errors
    catch {
        $error
        Write-Error "Can't delete archived config & resource file versions. See logs above."
        Log-Error "Can't delete archived config & resource file versions. See logs above."
    }
}
finally {
    $elapsedTime = $(get-date) - $deleteStartTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    Write-Host "Summary for this run: " -ForegroundColor Yellow
    $statusMap | Format-Table @{ Label = "Component"; Expression={$_.Name}}, @{n='Summary';e={
        if ($_.Value -is [Hashtable]) {
            $ht = $_.Value
            $a  = $ht.keys | sort | ForEach-Object { '{0}={1}' -f $_, $ht[$_] }
            '{{{0}}}' -f ($a -join ', ')
        } else {
            $_.Value
        }
    }}
    Write-Host "Total time taken: $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
 }