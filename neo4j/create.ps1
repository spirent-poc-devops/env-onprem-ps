#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "neo4j",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)

#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath


###################################################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    # Skip if already installed
    if (!(Get-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint")) {
        # Notify user about start of the task
        Write-Host "`n***** Started creating neo4j database. *****`n"
        Log-Info "Started creating neo4j database"

        # Prepare hosts file
        $ansible_inventory = @("[neo4j]")
        $neo4jVMCount = $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.nodes_count")
        for ($i = 0; $i -lt $neo4jVMCount; $i++) {
            $ansible_inventory += "$i ansible_host=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_private_key_path")"
        }
        Set-Content -Path "$path/../temp/neo4j_ansible_hosts" -Value $ansible_inventory

        # Whitelist nodes
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/neo4j_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/neo4j_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        }

        # Install neo4j database
        $neo4jVMCount = $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.nodes_count")
        $hw_neo4j_discovery_members = ""
        for ($i = 0; $i -lt $neo4jVMCount; $i++) {
            $hw_neo4j_discovery_members += "$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip"):5000,"
        }
        $hw_neo4j_discovery_members = $hw_neo4j_discovery_members.Substring(0, $hw_neo4j_discovery_members.Length - 1)

        $templateParams = @{ 
            hw_neo4j_discovery_members=$hw_neo4j_discovery_members
        }

        Build-EnvTemplate -InputPath "$($path)/templates/neo4j_install_playbook.yml" -OutputPath "$($path)/../temp/neo4j_install_playbook.yml" -Params1 $templateParams
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/neo4j_ansible_hosts" "$path/../temp/neo4j_install_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/neo4j_ansible_hosts" "$path/../temp/neo4j_install_playbook.yml"
        }

        # Write neo4j resources
        $port=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_port")
        $bolt_port=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_bolt_port")
        $ip=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_ip")
        $endpoint = "$($ip):$($port)"
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.host" -Value $ip
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.port" -Value $port
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.bolt_port" -Value $bolt_port
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint" -Value $endpoint
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.inventory" -Value $ansible_inventory
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
        $status = "CREATION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed creating neo4j database. *****`n"
        Log-Info "Completed creating neo4j database"
    } else
    {
        $status = "CREATION_SKIPPED"
        Write-Host "Neo4j component already installed. Installation skipped."
        Log-Info "Neo4j component already installed. Installation skipped."
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################
