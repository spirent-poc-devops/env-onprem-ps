#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "neo4j",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)

#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

###################################################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting neo4j database. *****`n"
        Log-Info "Started deleting neo4j database"

        # Prepare hosts file if missing
        if (!(Test-Path -Path "$path/../temp/neo4j_ansible_hosts")) {
            # Prepare hosts file
            $ansible_inventory = @("[neo4j]")
            $neo4jVMCount = $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.nodes_count")
            for ($i = 0; $i -lt $neo4jVMCount; $i++) {
                $ansible_inventory += "$i ansible_host=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_private_key_path")"
            }
            Set-Content -Path "$path/../temp/neo4j_ansible_hosts" -Value $ansible_inventory
        }

        Build-EnvTemplate -InputPath "$($path)/templates/neo4j_uninstall_playbook.yml" -OutputPath "$($path)/../temp/neo4j_uninstall_playbook.yml" -Params1 $templateParams
        # Destroy neo4j database
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/neo4j_ansible_hosts" "$path/../temp/neo4j_uninstall_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/neo4j_ansible_hosts" "$path/../temp/neo4j_uninstall_playbook.yml"
        }

        # Delete results and save resource file to disk
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.inventory"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.host"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.port"
        Remove-EnvMapValue -Map $resources -Key "$ResourcePrefix.bolt_port"
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting neo4j database. *****`n"
        Log-Info "Completed deleting neo4j database"
    }
    else 
    {
        $status = "DELETION_SKIPPED"
        exit 0
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)"
}

###################################################################
