#!/usr/bin/env pwsh

param
(
    [Alias("hwc", "HWConfig")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $HWConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $HWConfigPrefix = "hw",

    [Alias("hwr", "HWResources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $HWResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $HWResourcePrefix,

    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=4)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=5)]
    [string] $HWEnvironmentPrefix = "environment"
)

$env:LogComponent="hw"

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($HWResourcePath -eq $null) -or ($HWResourcePath -eq ""))
{
    $HWResourcePath = ConvertTo-EnvResourcePath -ConfigPath $HWConfigPath
}
if (($HWResourcePrefix -eq $null) -or ($HWResourcePrefix -eq "")) 
{ 
    $HWResourcePrefix = $HWConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $HWConfigPath
$resources = Read-EnvResources -ResourcePath $HWResourcePath

#Set Log Component 
$env:LogComponent = $HWConfigPrefix

################################## Timescale ##################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_0_id")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting timescale vitrtual machines. *****`n"
        Log-Info "Started deleting timescale vitrtual machines"

        # Prepare terraform files if they not exist
        $timescaleTerraformPath = "$path/../temp/timescale_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (!(Test-Path $timescaleTerraformPath)) {
            New-Item $timescaleTerraformPath -ItemType "directory"
            # Select cloud terraform templates
            $timescaleVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.nodes_count")
            for ($i = 0; $i -lt $timescaleVMCount; $i++) {
                switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                    "aws" {
                        $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                        $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                        $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                        $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                        $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                        $node_index = $i
                        $hw_timescale_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.instance_ami"
                        $hw_timescale_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.instance_type"
                        $hw_timescale_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.subnet_id"
                        $hw_timescale_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.keypair_name"

                        $templateParams = @{ 
                            hw_aws_region=$hw_aws_region;
                            hw_aws_cred_file=$hw_aws_cred_file; 
                            hw_aws_profile=$hw_aws_profile; 
                            hw_aws_vpc=$hw_aws_vpc; 
                            env_nameprefix=$env_nameprefix;
                            node_index=$node_index
                            hw_timescale_instance_ami=$hw_timescale_instance_ami; 
                            hw_timescale_instance_type=$hw_timescale_instance_type;
                            hw_timescale_subnet_id=$hw_timescale_subnet_id ; 
                            hw_timescale_keypair_name=$hw_timescale_keypair_name; 
                        }

                        Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$timescaleTerraformPath/provider.tf" -Params1 $templateParams
                        Build-EnvTemplate -InputPath "$($path)/templates/aws/timescale.tf" -OutputPath "$timescaleTerraformPath/timescale_$i.tf" -Params1 $templateParams
                    }
                    DEFAULT {
                        Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                        Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                    }
                }
            }
        } 

        # Delete aws timescale resources
        Set-Location $timescaleTerraformPath
        terraform init
        terraform plan
        terraform destroy -auto-approve
        Set-Location "$($path)/.."

        # Delete results and save resource file to disk
        $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 
        $timescaleVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.nodes_count")
        for ($i = 0; $i -lt $timescaleVMCount; $i++) {
            # Remove from resources
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_$($i)_private_ip"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_$($i)_public_ip"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_$($i)_id"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_$($i)_sg_id"

            # Remove from config
            try {
                $configOnprem.timescale."node_$($i)_username" = ""
                $configOnprem.timescale."node_$($i)_private_key_path" = ""
                $configOnprem.timescale."node_$($i)_ip" = ""
                $configOnprem.timescale."node_$($i)_port" = ""
            }
            catch {}
            
        }
        
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath

        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting timescale virtual machines. *****`n"
        Log-Info "Completed deleting timescale virtual machines"
    } else{
        $status = "DELETION_SKIPPED"
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "timescale.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "timescale.duration" -Value $totalTime
    }
    Write-Host "timescale virtual machines $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "timescale virtual machines $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################

################################## Neo4j ##################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_0_id")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting neo4j vitrtual machines. *****`n"
        Log-Info "Started deleting neo4j vitrtual machines"

        # Prepare terraform files if they not exist
        $neo4jTerraformPath = "$path/../temp/neo4j_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (!(Test-Path $neo4jTerraformPath)) {
            New-Item $neo4jTerraformPath -ItemType "directory"
            # Select cloud terraform templates
            $neo4jVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.nodes_count")
            for ($i = 0; $i -lt $neo4jVMCount; $i++) {
                switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                    "aws" {
                        $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                        $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                        $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                        $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                        $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                        $node_index = $i
                        $hw_neo4j_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.instance_ami"
                        $hw_neo4j_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.instance_type"
                        $hw_neo4j_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.subnet_id"
                        $hw_neo4j_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.keypair_name"

                        $templateParams = @{ 
                            hw_aws_region=$hw_aws_region;
                            hw_aws_cred_file=$hw_aws_cred_file; 
                            hw_aws_profile=$hw_aws_profile; 
                            hw_aws_vpc=$hw_aws_vpc; 
                            env_nameprefix=$env_nameprefix;
                            node_index=$node_index
                            hw_neo4j_instance_ami=$hw_neo4j_instance_ami; 
                            hw_neo4j_instance_type=$hw_neo4j_instance_type;
                            hw_neo4j_subnet_id=$hw_neo4j_subnet_id ; 
                            hw_neo4j_keypair_name=$hw_neo4j_keypair_name; 
                        }

                        Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$neo4jTerraformPath/provider.tf" -Params1 $templateParams
                        Build-EnvTemplate -InputPath "$($path)/templates/aws/neo4j.tf" -OutputPath "$neo4jTerraformPath/neo4j_$i.tf" -Params1 $templateParams
                    }
                    DEFAULT {
                        Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                        Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                    }
                }
            }
        } 

        # Delete aws neo4j resources
        Set-Location $neo4jTerraformPath
        terraform init
        terraform plan
        terraform destroy -auto-approve
        Set-Location "$($path)/.."

        # Delete results and save resource file to disk
        $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 
        $neo4jVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.nodes_count")
        for ($i = 0; $i -lt $neo4jVMCount; $i++) {
            # Remove from resources
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_$($i)_private_ip"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_$($i)_public_ip"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_$($i)_id"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_$($i)_sg_id"

            # Remove from config
            try {
                $configOnprem.neo4j."node_$($i)_username" = ""
                $configOnprem.neo4j."node_$($i)_private_key_path" = ""
                $configOnprem.neo4j."node_$($i)_ip" = ""
                $configOnprem.neo4j."node_$($i)_port" = ""
                $configOnprem.neo4j."node_$($i)_bolt_port" = ""
            }
            catch {}
            
        }
        
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath

        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting neo4j virtual machines. *****`n"
        Log-Info "Completed deleting neo4j virtual machines"
    } else{
        $status = "DELETION_SKIPPED"
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "neo4j.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "neo4j.duration" -Value $totalTime
    }
    Write-Host "Neo4j virtual machines $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Neo4j virtual machines $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################

################################## Kafka ##################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_0_id")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting kafka vitrtual machines. *****`n"
        Log-Info "Started deleting kafka vitrtual machines"

        # Prepare terraform files if they not exist
        $kafkaTerraformPath = "$path/../temp/kafka_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (!(Test-Path $kafkaTerraformPath)) {
            New-Item $kafkaTerraformPath -ItemType "directory"
            # Select cloud terraform templates
            $kafkaVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.nodes_count")
            for ($i = 0; $i -lt $kafkaVMCount; $i++) {
                switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                    "aws" {
                        $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                        $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                        $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                        $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                        $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                        $node_index = $i
                        $hw_kafka_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.instance_ami"
                        $hw_kafka_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.instance_type"
                        $hw_kafka_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.subnet_id"
                        $hw_kafka_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.keypair_name"

                        $templateParams = @{ 
                            hw_aws_region=$hw_aws_region;
                            hw_aws_cred_file=$hw_aws_cred_file; 
                            hw_aws_profile=$hw_aws_profile; 
                            hw_aws_vpc=$hw_aws_vpc; 
                            env_nameprefix=$env_nameprefix;
                            node_index=$node_index
                            hw_kafka_instance_ami=$hw_kafka_instance_ami; 
                            hw_kafka_instance_type=$hw_kafka_instance_type;
                            hw_kafka_subnet_id=$hw_kafka_subnet_id ; 
                            hw_kafka_keypair_name=$hw_kafka_keypair_name; 
                        }

                        Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$kafkaTerraformPath/provider.tf" -Params1 $templateParams
                        Build-EnvTemplate -InputPath "$($path)/templates/aws/kafka.tf" -OutputPath "$kafkaTerraformPath/kafka_$i.tf" -Params1 $templateParams
                    }
                    DEFAULT {
                        Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                        Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                    }
                }
            }
        } 

        # Delete aws kafka resources
        Set-Location $kafkaTerraformPath
        terraform init
        terraform plan
        terraform destroy -auto-approve
        Set-Location "$($path)/.."

        # Delete results and save resource file to disk
        $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 
        $kafkaVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.nodes_count")
        for ($i = 0; $i -lt $kafkaVMCount; $i++) {
            # Remove from resources
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_$($i)_private_ip"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_$($i)_public_ip"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_$($i)_id"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_$($i)_sg_id"

            # Remove from config
            try {
                $configOnprem.kafka."node_$($i)_username" = ""
                $configOnprem.kafka."node_$($i)_private_key_path" = ""
                $configOnprem.kafka."node_$($i)_ip" = ""
                $configOnprem.kafka."node_$($i)_port" = ""
            }
            catch {}
            
        }
        
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath

        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting kafka virtual machines. *****`n"
        Log-Info "Completed deleting kafka virtual machines"
    } else{
        $status = "DELETION_SKIPPED"
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "kafka.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "kafka.duration" -Value $totalTime
    }
    Write-Host "Kafka virtual machines $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Kafka virtual machines $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################


###################################################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.0_id")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting kubernetes virtual machines. *****`n"
        Log-Info "Started deleting kubernetes virtual machines."

        # Prepare terraform files if they not exist
        $k8sTerraformPath = "$path/../temp/k8s_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (!(Test-Path $k8sTerraformPath)) {
            New-Item $k8sTerraformPath -ItemType "directory"
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_access_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.access_id"
                    $hw_aws_access_key = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.access_key"
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $hw_k8s_subnet_zone = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_zone"
                    $hw_k8s_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_cidr"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $templateParams = @{ hw_aws_access_id=$hw_aws_access_id; hw_aws_access_key=$hw_aws_access_key; hw_aws_region=$hw_aws_region; hw_aws_vpc=$hw_aws_vpc; hw_k8s_subnet_zone=$hw_k8s_subnet_zone; hw_k8s_subnet_cidr=$hw_k8s_subnet_cidr; env_nameprefix=$env_nameprefix }
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$k8sTerraformPath/provider.tf" -Params1 $templateParams
                    # Delete kubernetes virtual machines
                    $k8sVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.nodes_count") + $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.masters_count")
                    for ($i = 0; $i -lt $k8sVMCount; $i++) {
                        Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.vm_index" = $i
                        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources

                        Build-EnvTemplate -InputPath "$($path)/templates/aws/k8s_vm.tf" -OutputPath "$k8sTerraformPath/k8s_vm_$i.tf" -Params1 $templateParams
                    }
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                }
            }
        } 

        # Delete aws k8s resources
        Set-Location $k8sTerraformPath
        terraform init
        terraform plan
        terraform destroy -auto-approve
        Set-Location "$($path)/.."
        
        $k8sVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.nodes_count") + $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.masters_count")
        for ($i = 0; $i -lt $k8sVMCount; $i++) {
            # Delete results and save resource file to disk
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_private_ip"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_public_ip"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_id"
            Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_sg_id"
            Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources

            # Delete k8s nodes ip from config file
            $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 
            $configOnprem.k8s."username" = ""
            $configOnprem.k8s."private_key_path" = ""
            $configOnprem.k8s."master_ips" = @()
            $configOnprem.k8s."worker_ips" = @()
            ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath
        }
        
        $resources.Remove("$HWResourcePrefix.k8s.subnet_id")
        $resources.Remove("$HWResourcePrefix.k8s.vm_index")
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting kubernetes virtual machines. *****`n"
        Log-Info "Completed deleting kubernetes virtual machines."
    } else{
        $status = "DELETION_SKIPPED"
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "k8s.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "k8s.duration" -Value $totalTime
    }
    Write-Host "Kubernetes virtual machines $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Kubernetes virtual machines $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################


###################################################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_state")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting kubernetes loadbalancer. *****`n"
        Log-Info "Started deleting kubernetes loadbalancer."

        # Prepare terraform files if they not exist
        $k8sLBTerraformPath = "$path/../temp/k8s_lb_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (!(Test-Path $k8sLBTerraformPath)) {
            New-Item $k8sLBTerraformPath -ItemType "directory"
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                    $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                    $hw_aws_account_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.account_id"
                    $hw_aws_role_name = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.terraform_role_name"
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $hw_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.subnet_cidr"
                    $hw_k8s_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_cidr"
                    $hw_k8s_lb_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.loadbalancer.instance_ami"
                    $hw_k8s_lb_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.loadbalancer.instance_type"
                    $hw_k8s_lb_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_id"
                    $hw_k8s_lb_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.keypair_name"
                    $hw_aws_security_group = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.security_group"
                    $k8s_masters_ids = ""
                    for($i=0; $i -lt $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.masters_count"); $i++ ) {
                        $k8s_masters_ids += "`"$(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_id")`"," 
                    }
                    $k8s_masters_ids = $k8s_masters_ids.Substring(0,$k8s_masters_ids.Length-1)
                    $templateParams = @{ 
                        hw_aws_cred_file=$hw_aws_cred_file
                        hw_aws_profile=$hw_aws_profile
                        hw_aws_account_id=$hw_aws_account_id; 
                        hw_aws_role_name=$hw_aws_role_name; 
                        hw_aws_region=$hw_aws_region; 
                        env_nameprefix=$env_nameprefix;
                        k8s_masters_ids=$k8s_masters_ids;
                        hw_aws_vpc=$hw_aws_vpc
                        hw_mgmt_subnet_cidr=$hw_mgmt_subnet_cidr
                        hw_k8s_subnet_cidr=$hw_k8s_subnet_cidr
                        hw_k8s_lb_instance_ami=$hw_k8s_lb_instance_ami
                        hw_k8s_lb_instance_type=$hw_k8s_lb_instance_type
                        hw_k8s_lb_subnet_id=$hw_k8s_lb_subnet_id
                        hw_k8s_lb_keypair_name=$hw_k8s_lb_keypair_name
                        hw_aws_security_group=$hw_aws_security_group
                    }
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$k8sLBTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/k8s_lb.tf" -OutputPath "$k8sLBTerraformPath/k8s_lb.tf" -Params1 $templateParams
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                }
            }
        } 

        # Delete aws k8s resources
        Set-Location $k8sLBTerraformPath
        terraform init
        terraform plan
        terraform destroy -auto-approve
        Set-Location "$($path)/.."
    
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_state"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_id"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_private_ip"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_public_ip"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_sg_id"
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting kubernetes loadbalancer. *****`n"
        Log-Info "Completed deleting kubernetes loadbalancer."
    } else{
        $status = "DELETION_SKIPPED"
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "k8s_lb.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "k8s_lb.duration" -Value $totalTime
    }
    Write-Host "Kubernetes loadbalancer $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Kubernetes loadbalancer $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################

################################## Management station ##################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.private_ip")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting management station. *****`n"
        Log-Info "Started deleting management station."

        # Prepare terraform files if they not exist
        $mgmtTerraformPath = "$path/../temp/mgmt_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (!(Test-Path $mgmtTerraformPath)) {
            New-Item $mgmtTerraformPath -ItemType "directory"
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_access_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.access_id"
                    $hw_aws_access_key = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.access_key"
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $hw_mgmt_subnet_zone = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.subnet_zone"
                    $hw_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.subnet_cidr"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $hw_mgmt_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.instance_ami"
                    $hw_mgmt_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.instance_type"
                    $hw_mgmt_keypair_name = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.keypair_name"
                    $hw_mgmt_ssh_allowed_cidr_blocks = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.ssh_allowed_cidr_blocks"
                    $templateParams = @{ hw_aws_access_id=$hw_aws_access_id; hw_aws_access_key=$hw_aws_access_key; hw_aws_region=$hw_aws_region; hw_aws_vpc=$hw_aws_vpc; hw_mgmt_subnet_zone=$hw_mgmt_subnet_zone; hw_mgmt_subnet_cidr=$hw_mgmt_subnet_cidr; env_nameprefix=$env_nameprefix; hw_mgmt_instance_ami=$hw_mgmt_instance_ami; hw_mgmt_instance_type=$hw_mgmt_instance_type; hw_mgmt_keypair_name=$hw_mgmt_keypair_name  }
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$mgmtTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/mgmt.tf" -OutputPath "$mgmtTerraformPath/mgmt.tf" -Params1 $templateParams
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                }
            }
        } 

        # Delete aws mgmt resources
        Set-Location $mgmtTerraformPath
        terraform init
        terraform plan
        terraform destroy -auto-approve
        Set-Location "$($path)/.."

        # Delete results and save resource file to disk
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.private_ip"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.public_ip"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.id"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.sg_id"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.ssh_cmd"
        Remove-EnvMapValue -Map $resources -Key "$HWEnvironmentPrefix.prefix"
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources

        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting management station. *****`n"
        Log-Info "Completed deleting management station."
    } else{
        $status = "DELETION_SKIPPED"
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "k8s_lb.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "k8s_lb.duration" -Value $totalTime
    }
    Write-Host "Management Station $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Management Station $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################

################################## Keypair ##################################
try{
    $status = "DELETION_STARTED"
    $startTime = $(get-date)
    # Skip if resource wasn't created
    if (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.aws.keypair_name")
    {
        # Notify user about start of the task
        Write-Host "`n***** Started deleting keypair. *****`n"
        Log-Info "Started deleting keypair."

        # Prepare terraform files if they not exist
        $keypairTerraformPath = "$path/../temp/keypair_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (!(Test-Path $keypairTerraformPath)) {
            New-Item $keypairTerraformPath -ItemType "directory"
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                    $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $hw_aws_stage = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.stage"
                    $hw_aws_namespace = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.namespace"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"

                    $templateParams = @{ 
                        hw_aws_cred_file=$hw_aws_cred_file; 
                        hw_aws_profile=$hw_aws_profile; 
                        hw_aws_region=$hw_aws_region; 
                        hw_aws_stage=$hw_aws_stage; 
                        hw_aws_namespace=$hw_aws_namespace; 
                        env_nameprefix=$env_nameprefix 
                    }
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$keypairTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/keypair.tf" -OutputPath "$keypairTerraformPath/keypair.tf" -Params1 $templateParams   
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") doesn't supported."
                }
            }
        } 

        # Delete aws mgmt resources
        Set-Location $keypairTerraformPath
        terraform init
        terraform plan
        terraform destroy -auto-approve
        Set-Location "$($path)/.."

        # Delete results and save resource file to disk
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.keypair_name"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.keypair_name"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.keypair_name"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.keypair_name"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.keypair_name"
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.aws.keypair_name"
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources

        $status = "DELETION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed deleting keypair. *****`n"
        Log-Info "Completed deleting keypair."
    } else {
        $status = "DELETION_SKIPPED"
    }
}
finally {
    if ($status -eq "DELETION_STARTED"){
        $status = "DELETION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "keypair.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "keypair.duration" -Value $totalTime
    }
    Write-Host "Keypairs $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Keypairs $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################