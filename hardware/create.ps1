#!/usr/bin/env pwsh
param
(
    [Alias("hwc", "HWConfig")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $HWConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $HWConfigPrefix = "hw",

    [Alias("hwr", "HWResources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $HWResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $HWResourcePrefix,

    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=4)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=5)]
    [string] $HWEnvironmentPrefix = "environment"
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($HWResourcePath -eq $null) -or ($HWResourcePath -eq ""))
{
    $HWResourcePath = ConvertTo-EnvResourcePath -ConfigPath $HWConfigPath
}
if (($HWResourcePrefix -eq $null) -or ($HWResourcePrefix -eq "")) 
{ 
    $HWResourcePrefix = $HWConfigPrefix 
}

$env:LogComponent="hw"

# Read config and resources
$config = Read-EnvConfig -ConfigPath $HWConfigPath
$resources = Read-EnvResources -ResourcePath $HWResourcePath
# Save env_nameprefix without dashes (terraform suggests use resource names without dashes)
Set-EnvMapValue -Map $resources -Key "$HWEnvironmentPrefix.prefix" -Value $($(Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix").replace("-","_"))

# Configure AWS cli
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
$env:TF_VAR_terraform_role_name = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.terraform_role_name"
$env:TF_VAR_account_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.account_id"

############################## Keypair #####################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    
    if (!(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.aws.keypair_name")) {
        # Notify user about start of the task
        Write-Host "`n***** Started creating keypair. *****`n"
        Log-Info "Started creating keypair"
        
        # Prepare terraform files
        $keypairTerraformPath = "$path/../temp/keypair_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (Test-Path $keypairTerraformPath) {
            Remove-Item $keypairTerraformPath -Recurse -Force
        } 
        $null = New-Item $keypairTerraformPath -ItemType "directory"

        # Select cloud terraform templates
        switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
            "aws" {
                $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                $hw_aws_stage = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.stage"
                $hw_aws_namespace = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.namespace"
                $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"

                $templateParams = @{ 
                    hw_aws_cred_file=$hw_aws_cred_file; 
                    hw_aws_profile=$hw_aws_profile; 
                    hw_aws_region=$hw_aws_region; 
                    hw_aws_stage=$hw_aws_stage; 
                    hw_aws_namespace=$hw_aws_namespace; 
                    env_nameprefix=$env_nameprefix 
                }

                Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$keypairTerraformPath/provider.tf" -Params1 $templateParams
                Build-EnvTemplate -InputPath "$($path)/templates/aws/keypair.tf" -OutputPath "$keypairTerraformPath/keypair.tf" -Params1 $templateParams
            }
            DEFAULT {
                Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported"
            }
        }

        # Initialize terraform
        Set-Location $keypairTerraformPath
        Write-Host "Initializing terraform..."
        Log-Info "Initializing terraform"
        terraform init
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't initialize terraform. Watch logs above or check $keypairTerraformPath folder content."
            Log-Error "Can't initialize terraform. Watch logs above or check $keypairTerraformPath folder content"
        }

        # Plan cloud resources creation
        Write-Host "Executing terraform plan command..."
        Log-Info "Executing terraform plan command"
        terraform plan
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't execute terraform plan. Watch logs above or check $keypairTerraformPath folder content."
            Log-Error "Can't execute terraform plan. Watch logs above or check $keypairTerraformPath folder content"
        }

        # Create cloud resources
        Write-Host "Creating cloud resources by terraform..."
        Log-Info "Creating cloud resources by terraform"
        terraform apply -auto-approve
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't create cloud resources. Watch logs above or check $keypairTerraformPath folder content."
            Log-Error "Can't create cloud resources. Watch logs above or check $keypairTerraformPath folder content"
        }

        # Get keypair name
        $keypair=$(terraform output keypair_name).Trim('"')
        Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.keypair_name" -Value $keypair
        Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.keypair_name" -Value $keypair
        Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.keypair_name" -Value $keypair
        Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.keypair_name" -Value $keypair
        Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.keypair_name" -Value $keypair
        Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.aws.keypair_name" -Value $keypair
        Remove-Item .terraform -Recurse -ErrorAction Ignore
        Set-Location "$($path)/.."
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        $status = "CREATION_COMPLETED"
        Write-Host "`n***** Completed creating keypair. *****`n"
        Log-Info "Completed creating keypair."
    } else {
        $keypairTerraformPath = "$path/../temp/keypair_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        $status = "CREATION_SKIPPED"
        Write-Host "Keypair already created"
        Log-Info "Keypair already created"
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "keypair.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "keypair.duration" -Value $totalTime
    }
    Write-Host "Keypair $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Keypair $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################

################################## Kubernetes Virtual Machines #################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    if (!(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.0_id")) {

        # Prepare terraform files
        $k8sTerraformPath = "$path/../temp/k8s_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (Test-Path $k8sTerraformPath) {
            Remove-Item $k8sTerraformPath -Recurse -Force
        } 
        $null = New-Item $k8sTerraformPath -ItemType "directory"

        # Notify user about start of the task
        Write-Host "`n***** Started creating kubernetes virtual machines. *****`n"
        Log-Info "Started creating kubernetes virtual machines"
        $k8sVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.nodes_count") + $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.masters_count")
        for ($i = 0; $i -lt $k8sVMCount; $i++) {
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.vm_index" -Value $i
            Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
            
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                    $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                    $hw_aws_account_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.account_id"
                    $hw_aws_role_name = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.terraform_role_name"
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $hw_k8s_vm_index = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.vm_index"
                    $hw_k8s_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.instance_ami"
                    $hw_k8s_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.instance_type"
                    $hw_k8s_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.keypair_name"
                    $hw_k8s_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_id"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $hw_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.subnet_cidr"
                    $hw_k8s_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_cidr"
                    $hw_aws_security_group = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.security_group"
                    $hw_k8s_volume_size = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.volume_size"

                    $templateParams = @{ 
                        hw_aws_cred_file=$hw_aws_cred_file; 
                        hw_aws_profile=$hw_aws_profile; 
                        hw_aws_account_id=$hw_aws_account_id; 
                        hw_aws_role_name=$hw_aws_role_name;
                        hw_aws_region=$hw_aws_region; 
                        hw_k8s_vm_index=$hw_k8s_vm_index; 
                        hw_k8s_instance_ami=$hw_k8s_instance_ami; 
                        hw_k8s_instance_type=$hw_k8s_instance_type; 
                        hw_k8s_keypair_name=$hw_k8s_keypair_name; 
                        hw_k8s_subnet_id=$hw_k8s_subnet_id; 
                        env_nameprefix=$env_nameprefix; 
                        hw_aws_vpc=$hw_aws_vpc; 
                        hw_mgmt_subnet_cidr=$hw_mgmt_subnet_cidr; 
                        hw_aws_security_group=$hw_aws_security_group; 
                        hw_k8s_subnet_cidr=$hw_k8s_subnet_cidr;
                        hw_k8s_volume_size=$hw_k8s_volume_size
                    }
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$k8sTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/k8s_vm.tf" -OutputPath "$k8sTerraformPath/k8s_vm_$i.tf" -Params1 $templateParams
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                }
            }
        }

        # Initialize terraform
        Set-Location $k8sTerraformPath
        Write-Host "Initializing terraform..."
        Log-Info "Initializing terraform"
        terraform init
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't initialize terraform. Watch logs above or check $k8sTerraformPath folder content."
            Log-Error "Can't initialize terraform. Watch logs above or check $k8sTerraformPath folder content"
        }

        # Plan cloud resources creation
        Write-Host "Executing terraform plan command..."
        Log-Info "Executing terraform plan command"
        terraform plan
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't execute terraform plan. Watch logs above or check $k8sTerraformPath folder content."
            Log-Error "Can't execute terraform plan. Watch logs above or check $k8sTerraformPath folder content"
        }

        # Create cloud resources
        Write-Host "Creating cloud resources by terraform..."
        Log-Info "Creating cloud resources by terraform"
        terraform apply -auto-approve
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't create cloud resources. Watch logs above or check $k8sTerraformPath folder content."
            Log-Error "Can't create cloud resources. Watch logs above or check $k8sTerraformPath folder content"
        }

        for ($i = 0; $i -lt $k8sVMCount; $i++) {
            # Save results to resource file
            $k8s_id=$(terraform output "k8s_$i`_id").Trim('"')
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_id" -Value $k8s_id
            $k8s_private_ip=$(terraform output "k8s_$i`_private_ip").Trim('"') 
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_private_ip" -Value $k8s_private_ip
            $k8s_public_ip=$(terraform output "k8s_$i`_public_ip").Trim('"')
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_public_ip" -Value $k8s_public_ip
            $k8s_sg_id=$(terraform output "k8s_$i`_sg_id").Trim('"')
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_sg_id" -Value $k8s_sg_id
        }
        Set-Location $k8sTerraformPath
        Remove-Item .terraform -Recurse -ErrorAction Ignore
        Set-Location "$($path)/.."
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources

        # Remove temporary variable from resource file
        Remove-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.vm_index"
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        # Save k8s nodes ip to config file
        $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 
        $configOnprem.k8s | add-member -Name "master_ips" -Value @() -MemberType NoteProperty -Force
        $configOnprem.k8s | add-member -Name "worker_ips" -Value @() -MemberType NoteProperty -Force
        $configOnprem.k8s | add-member -Name "username" -Value (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.username") -MemberType NoteProperty -Force
        $configOnprem.k8s | add-member -Name "private_key_path" -Value "config/$(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.keypair_name").pem" -MemberType NoteProperty -Force
        for ($i = 0; $i -lt $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.masters_count"); $i++) {
            $configOnprem.k8s."master_ips" += (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_private_ip")
        }
        for ($i = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.masters_count"); $i -lt $k8sVMCount; $i++) {
            $configOnprem.k8s."worker_ips" += (Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_private_ip")
        }
        ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath
        $status = "CREATION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed creating kubernetes virtual machines. *****`n"
        Log-Info "Completed creating kubernetes virtual machines"
    } else {
        $status = "CREATION_SKIPPED"
        Write-Host "K8S virtual machines already created."
        Log-Info "K8S virtual machines already created"
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "k8s_vm.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "k8s_vm.duration" -Value $totalTime
    }
    Write-Host "K8S virtual machines $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "K8S virtual machines $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################

############################### k8s loadbalancer ####################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    if ($(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.masters_count") -gt 1) {
        if (!(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_state")) {

            # Prepare terraform files
            $k8sLBTerraformPath = "$path/../temp/k8s_lb_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
            if (Test-Path $k8sLBTerraformPath) {
                Remove-Item $k8sLBTerraformPath -Recurse -Force
            } 
            $null = New-Item $k8sLBTerraformPath -ItemType "directory"

            # Notify user about start of the task
            Write-Host "`n***** Started creating k8s loadbalancer. *****`n"
            Log-Info "Started creating k8s loadbalancer"

            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                    $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                    $hw_aws_account_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.account_id"
                    $hw_aws_role_name = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.terraform_role_name"
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $hw_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.subnet_cidr"
                    $hw_k8s_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_cidr"
                    $hw_k8s_lb_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.loadbalancer.instance_ami"
                    $hw_k8s_lb_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.loadbalancer.instance_type"
                    $hw_k8s_lb_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_id"
                    $hw_k8s_lb_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.keypair_name"
                    $hw_aws_security_group = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.security_group"
                    $k8s_masters_ids = ""
                    for($i=0; $i -lt $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.masters_count"); $i++ ) {
                        $k8s_masters_ids += "`"$(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.$i`_id")`"," 
                    }
                    $k8s_masters_ids = $k8s_masters_ids.Substring(0,$k8s_masters_ids.Length-1)
                    $templateParams = @{ 
                        hw_aws_cred_file=$hw_aws_cred_file
                        hw_aws_profile=$hw_aws_profile
                        hw_aws_account_id=$hw_aws_account_id; 
                        hw_aws_role_name=$hw_aws_role_name; 
                        hw_aws_region=$hw_aws_region; 
                        env_nameprefix=$env_nameprefix;
                        k8s_masters_ids=$k8s_masters_ids;
                        hw_aws_vpc=$hw_aws_vpc
                        hw_mgmt_subnet_cidr=$hw_mgmt_subnet_cidr
                        hw_k8s_subnet_cidr=$hw_k8s_subnet_cidr
                        hw_k8s_lb_instance_ami=$hw_k8s_lb_instance_ami
                        hw_k8s_lb_instance_type=$hw_k8s_lb_instance_type
                        hw_k8s_lb_subnet_id=$hw_k8s_lb_subnet_id
                        hw_k8s_lb_keypair_name=$hw_k8s_lb_keypair_name
                        hw_aws_security_group=$hw_aws_security_group
                    }
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$k8sLBTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/k8s_lb.tf" -OutputPath "$k8sLBTerraformPath/k8s_lb.tf" -Params1 $templateParams
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                }
            }

            # Initialize terraform
            Set-Location $k8sLBTerraformPath

            Write-Host "Initializing terraform..."
            Log-Info "Initializing terraform"
            terraform init
            # Check for error
            if ($LastExitCode -ne 0) {
                Set-Location "$($path)/.."
                Write-Error "Can't initialize terraform. Watch logs above or check $k8sLBTerraformPath folder content."
                Log-Error "Can't initialize terraform. Watch logs above or check $k8sLBTerraformPath folder content."
            }

            # Plan cloud resources creation
            Write-Host "Executing terraform plan command..."
            Log-Info "Executing terraform plan command"
            terraform plan
            # Check for error
            if ($LastExitCode -ne 0) {
                Set-Location "$($path)/.."
                Write-Error "Can't execute terraform plan. Watch logs above or check $k8sLBTerraformPath folder content."
                Log-Error "Can't execute terraform plan. Watch logs above or check $k8sLBTerraformPath folder content."
            }

            # Create cloud resources
            Write-Host "Creating cloud resources by terraform..."
            Log-Info "Creating cloud resources by terraform"
            terraform apply -auto-approve
            # Check for error
            if ($LastExitCode -ne 0) {
                Set-Location "$($path)/.."
                Write-Error "Can't create cloud resources. Watch logs above or check $k8sLBTerraformPath folder content."
                Log-Error "Can't create cloud resources. Watch logs above or check $k8sLBTerraformPath folder content."
            }

            # Create k8s loadbalancer outputs to resources
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_state" -Value "deployed"
            $k8s_lb_id=$(terraform output k8s_lb_id).Trim('"')
            $k8s_lb_private_ip=$(terraform output k8s_lb_private_ip).Trim('"')
            $k8s_lb_public_ip=$(terraform output k8s_lb_public_ip).Trim('"')
            $k8s_lb_sg_id=$(terraform output k8s_lb_sg_id).Trim('"')
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_id" -Value $k8s_lb_id 
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_private_ip" -Value $k8s_lb_private_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_public_ip" -Value $k8s_lb_public_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.k8s.lb_sg_id" -Value $k8s_lb_sg_id
            Set-Location "$($path)/.."
            Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources

            # Save k8s loadbalancer ip to config file
            $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 
            $configOnprem.k8s.loadbalancer.host = $k8s_lb_private_ip
            ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath
            
            $status = "CREATION_COMPLETED"
            
            Write-Host "`n***** Completed creating k8s loadbalancer. *****`n"
            Log-Info "Completed creating k8s loadbalancer"
        } else {
            $status = "CREATION_SKIPPED"
            Write-Host "Kubernetes loadbalancer already created."
            Log-Info "Kubernetes loadbalancer already created"
        }
    } else{
        $status = "CREATION_SKIPPED"
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "k8s_lb.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "k8s_lb.duration" -Value $totalTime
    }
    Write-Host "Kubernetes loadbalancer $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Kubernetes loadbalancer $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################

############################### Timescale ####################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    ###################################################################
    if (!(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_0_id")) {
        # Notify user about start of the task
        Write-Host "`n***** Started creating timescale instances. *****`n"
        Log-Info "Started creating timescale instances"

        # Prepare terraform files
        $timescaleTerraformPath = "$path/../temp/timescale_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (Test-Path $timescaleTerraformPath) {
            Remove-Item $timescaleTerraformPath -Recurse -Force
        } 
        $null = New-Item $timescaleTerraformPath -ItemType "directory"

        # Read config (required to update it after hardware created)
        $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 

        $timescaleVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.nodes_count")
        for ($i = 0; $i -lt $timescaleVMCount; $i++) {
                
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                    $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $node_index = $i
                    $hw_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.subnet_cidr"
                    $hw_k8s_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.k8s.subnet_cidr"
                    $hw_timescale_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.subnet_cidr"
                    $hw_timescale_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.instance_ami"
                    $hw_timescale_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.instance_type"
                    $hw_timescale_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.subnet_id"
                    $hw_timescale_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.keypair_name"
                    $hw_timescale_volume_size = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.volume_size"

                    $templateParams = @{ 
                        hw_aws_region=$hw_aws_region;
                        hw_aws_cred_file=$hw_aws_cred_file; 
                        hw_aws_profile=$hw_aws_profile; 
                        hw_aws_vpc=$hw_aws_vpc; 
                        env_nameprefix=$env_nameprefix;
                        node_index=$node_index
                        hw_mgmt_subnet_cidr=$hw_mgmt_subnet_cidr
                        hw_k8s_subnet_cidr=$hw_k8s_subnet_cidr
                        hw_timescale_subnet_cidr=$hw_timescale_subnet_cidr
                        hw_timescale_instance_ami=$hw_timescale_instance_ami; 
                        hw_timescale_instance_type=$hw_timescale_instance_type;
                        hw_timescale_subnet_id=$hw_timescale_subnet_id ; 
                        hw_timescale_keypair_name=$hw_timescale_keypair_name; 
                        hw_timescale_volume_size=$hw_timescale_volume_size
                    }

                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$timescaleTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/timescale.tf" -OutputPath "$timescaleTerraformPath/timescale_$i.tf" -Params1 $templateParams
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                }
            }
        }

        # Initialize terraform
        Set-Location $timescaleTerraformPath
        Write-Host "Initializing terraform..."
        Log-Info "Initializing terraform"
        terraform init
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't initialize terraform. Watch logs above or check $timescaleTerraformPath folder content."
            Log-Error "Can't initialize terraform. Watch logs above or check $timescaleTerraformPath folder content."
        }

        # Plan cloud resources creation
        Write-Host "Executing terraform plan command..."
        Log-Info "Executing terraform plan command"
        terraform plan
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't execute terraform plan. Watch logs above or check $timescaleTerraformPath folder content."
            Log-Error "Can't execute terraform plan. Watch logs above or check $timescaleTerraformPath folder content."
        }

        # Create cloud resources
        Write-Host "Creating cloud resources by terraform..."
        Log-Info "Creating cloud resources by terraform"
        terraform apply -auto-approve
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't create cloud resources. Watch logs above or check $timescaleTerraformPath folder content."
            Log-Error "Can't create cloud resources. Watch logs above or check $timescaleTerraformPath folder content."
        }


        for ($i = 0; $i -lt $timescaleVMCount; $i++) {
            # Get outputs
            $timescale_id=$(terraform output "timescale_$($i)_id").Trim('"')
            $timescale_private_ip=$(terraform output "timescale_$($i)_private_ip").Trim('"')
            $timescale_public_ip=$(terraform output "timescale_$($i)_public_ip").Trim('"')
            $timescale_sg_id=$(terraform output "timescale_$($i)_sg_id").Trim('"')

            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_$($i)_id" -Value $timescale_id
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_$($i)_private_ip" -Value $timescale_private_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_$($i)_public_ip" -Value $timescale_public_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.node_$($i)_sg_id" -Value $timescale_sg_id

            # Save timescale instance ip to config file
            $configOnprem.timescale | add-member -Name "node_$($i)_ip" -Value $timescale_private_ip -MemberType NoteProperty -Force
            $configOnprem.timescale | add-member -Name "node_$($i)_port" -Value "5432" -MemberType NoteProperty -Force
            $configOnprem.timescale | add-member -Name "node_$($i)_username" -Value (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.timescale.username") -MemberType NoteProperty -Force
            $configOnprem.timescale | add-member -Name "node_$($i)_private_key_path" -Value "config/$(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.timescale.keypair_name").pem" -MemberType NoteProperty -Force
        }

        # Remove temporary terraform files
        Remove-Item .terraform -Recurse -ErrorAction Ignore
        Set-Location "$($path)/.."
        # Save resources and config files
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath

        $status = "CREATION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed creating timescale virtual machines. *****`n"
        Log-Info "Completed creating timescale virtual machines"
    } else {
        $status = "CREATION_SKIPPED"
        Write-Host "Timescale virtual machines already created."
        Log-Info "Timescale virtual machines already created"
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "timescale.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "timescale.duration" -Value $totalTime
    }
    Write-Host "Timescale virtual machines $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Timescale virtual machines $status in $totalTime (HH:mm:ss.ms)"
}
#####################################################################################

############################### Neo4j ####################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    ###################################################################
    if (!(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_0_id")) {
        # Notify user about start of the task
        Write-Host "`n***** Started creating neo4j instances. *****`n"
        Log-Info "Started creating neo4j instances"

        # Prepare terraform files
        $neo4jTerraformPath = "$path/../temp/neo4j_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (Test-Path $neo4jTerraformPath) {
            Remove-Item $neo4jTerraformPath -Recurse -Force
        } 
        $null = New-Item $neo4jTerraformPath -ItemType "directory"

        # Read config (required to update it after hardware created)
        $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 

        $neo4jVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.nodes_count")
        for ($i = 0; $i -lt $neo4jVMCount; $i++) {
                
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                    $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $node_index = $i
                    $hw_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.subnet_cidr"
                    $hw_neo4j_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.subnet_cidr"
                    $hw_neo4j_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.instance_ami"
                    $hw_neo4j_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.instance_type"
                    $hw_neo4j_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.subnet_id"
                    $hw_neo4j_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.keypair_name"
                    $hw_neo4j_volume_size = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.volume_size"

                    $templateParams = @{ 
                        hw_aws_region=$hw_aws_region;
                        hw_aws_cred_file=$hw_aws_cred_file; 
                        hw_aws_profile=$hw_aws_profile; 
                        hw_aws_vpc=$hw_aws_vpc; 
                        env_nameprefix=$env_nameprefix;
                        node_index=$node_index
                        hw_neo4j_subnet_cidr=$hw_neo4j_subnet_cidr
                        hw_mgmt_subnet_cidr=$hw_mgmt_subnet_cidr
                        hw_neo4j_instance_ami=$hw_neo4j_instance_ami; 
                        hw_neo4j_instance_type=$hw_neo4j_instance_type;
                        hw_neo4j_subnet_id=$hw_neo4j_subnet_id ; 
                        hw_neo4j_keypair_name=$hw_neo4j_keypair_name; 
                        hw_neo4j_volume_size=$hw_neo4j_volume_size
                    }

                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$neo4jTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/neo4j.tf" -OutputPath "$neo4jTerraformPath/neo4j_$i.tf" -Params1 $templateParams
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                }
            }
        }

        # Initialize terraform
        Set-Location $neo4jTerraformPath
        Write-Host "Initializing terraform..."
        Log-Info "Initializing terraform"
        terraform init
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't initialize terraform. Watch logs above or check $neo4jTerraformPath folder content."
            Log-Error "Can't initialize terraform. Watch logs above or check $neo4jTerraformPath folder content."
        }

        # Plan cloud resources creation
        Write-Host "Executing terraform plan command..."
        Log-Info "Executing terraform plan command"
        terraform plan
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't execute terraform plan. Watch logs above or check $neo4jTerraformPath folder content."
            Log-Error "Can't execute terraform plan. Watch logs above or check $neo4jTerraformPath folder content."
        }

        # Create cloud resources
        Write-Host "Creating cloud resources by terraform..."
        Log-Info "Creating cloud resources by terraform"
        terraform apply -auto-approve
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't create cloud resources. Watch logs above or check $neo4jTerraformPath folder content."
            Log-Error "Can't create cloud resources. Watch logs above or check $neo4jTerraformPath folder content."
        }


        for ($i = 0; $i -lt $neo4jVMCount; $i++) {
            # Get outputs
            $neo4j_id=$(terraform output "neo4j_$($i)_id").Trim('"')
            $neo4j_private_ip=$(terraform output "neo4j_$($i)_private_ip").Trim('"')
            $neo4j_public_ip=$(terraform output "neo4j_$($i)_public_ip").Trim('"')
            $neo4j_sg_id=$(terraform output "neo4j_$($i)_sg_id").Trim('"')

            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_$($i)_id" -Value $neo4j_id
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_$($i)_private_ip" -Value $neo4j_private_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_$($i)_public_ip" -Value $neo4j_public_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.node_$($i)_sg_id" -Value $neo4j_sg_id

            # Save neo4j instance ip to config file
            $configOnprem.neo4j | add-member -Name "node_$($i)_ip" -Value $neo4j_private_ip -MemberType NoteProperty -Force
            $configOnprem.neo4j | add-member -Name "node_$($i)_port" -Value "7474" -MemberType NoteProperty -Force
            $configOnprem.neo4j | add-member -Name "node_$($i)_bolt_port" -Value "7687" -MemberType NoteProperty -Force
            $configOnprem.neo4j | add-member -Name "node_$($i)_username" -Value (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.neo4j.username") -MemberType NoteProperty -Force
            $configOnprem.neo4j | add-member -Name "node_$($i)_private_key_path" -Value "config/$(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.neo4j.keypair_name").pem" -MemberType NoteProperty -Force
        }

        # Remove temporary terraform files
        Remove-Item .terraform -Recurse -ErrorAction Ignore
        Set-Location "$($path)/.."
        # Save resources and config files
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath

        $status = "CREATION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed creating neo4j virtual machines. *****`n"
        Log-Info "Completed creating neo4j virtual machines"
    } else {
        $status = "CREATION_SKIPPED"
        Write-Host "Neo4j virtual machines already created."
        Log-Info "Neo4j virtual machines already created"
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "neo4j.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "neo4j.duration" -Value $totalTime
    }
    Write-Host "Neo4j virtual machines $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Neo4j virtual machines $status in $totalTime (HH:mm:ss.ms)"
}
#####################################################################################

############################### Kafka ####################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    ###################################################################
    if (!(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_0_id")) {
        # Notify user about start of the task
        Write-Host "`n***** Started creating kafka instances. *****`n"
        Log-Info "Started creating kafka instances"

        # Prepare terraform files
        $kafkaTerraformPath = "$path/../temp/kafka_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
        if (Test-Path $kafkaTerraformPath) {
            Remove-Item $kafkaTerraformPath -Recurse -Force
        } 
        $null = New-Item $kafkaTerraformPath -ItemType "directory"

        # Read config (required to update it after hardware created)
        $configOnprem = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json 

        $kafkaVMCount = $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.nodes_count")
        for ($i = 0; $i -lt $kafkaVMCount; $i++) {
                
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                    $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $node_index = $i
                    $hw_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.subnet_cidr"
                    $hw_kafka_subnet_cidr = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.subnet_cidr"
                    $hw_kafka_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.instance_ami"
                    $hw_kafka_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.instance_type"
                    $hw_kafka_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.subnet_id"
                    $hw_kafka_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.keypair_name"
                    $hw_kafka_volume_size = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.volume_size"

                    $templateParams = @{ 
                        hw_aws_region=$hw_aws_region;
                        hw_aws_cred_file=$hw_aws_cred_file; 
                        hw_aws_profile=$hw_aws_profile; 
                        hw_aws_vpc=$hw_aws_vpc; 
                        env_nameprefix=$env_nameprefix;
                        node_index=$node_index
                        hw_mgmt_subnet_cidr=$hw_mgmt_subnet_cidr
                        hw_kafka_subnet_cidr=$hw_kafka_subnet_cidr
                        hw_kafka_instance_ami=$hw_kafka_instance_ami; 
                        hw_kafka_instance_type=$hw_kafka_instance_type;
                        hw_kafka_subnet_id=$hw_kafka_subnet_id ; 
                        hw_kafka_keypair_name=$hw_kafka_keypair_name; 
                        hw_kafka_volume_size=$hw_kafka_volume_size
                    }

                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$kafkaTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/kafka.tf" -OutputPath "$kafkaTerraformPath/kafka_$i.tf" -Params1 $templateParams
                }
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                }
            }
        }

        # Initialize terraform
        Set-Location $kafkaTerraformPath
        Write-Host "Initializing terraform..."
        Log-Info "Initializing terraform"
        terraform init
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't initialize terraform. Watch logs above or check $kafkaTerraformPath folder content."
            Log-Error "Can't initialize terraform. Watch logs above or check $kafkaTerraformPath folder content."
        }

        # Plan cloud resources creation
        Write-Host "Executing terraform plan command..."
        Log-Info "Executing terraform plan command"
        terraform plan
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't execute terraform plan. Watch logs above or check $kafkaTerraformPath folder content."
            Log-Error "Can't execute terraform plan. Watch logs above or check $kafkaTerraformPath folder content."
        }

        # Create cloud resources
        Write-Host "Creating cloud resources by terraform..."
        Log-Info "Creating cloud resources by terraform"
        terraform apply -auto-approve
        # Check for error
        if ($LastExitCode -ne 0) {
            Set-Location "$($path)/.."
            Write-Error "Can't create cloud resources. Watch logs above or check $kafkaTerraformPath folder content."
            Log-Error "Can't create cloud resources. Watch logs above or check $kafkaTerraformPath folder content."
        }


        for ($i = 0; $i -lt $kafkaVMCount; $i++) {
            # Get outputs
            $kafka_id=$(terraform output "kafka_$($i)_id").Trim('"')
            $kafka_private_ip=$(terraform output "kafka_$($i)_private_ip").Trim('"')
            $kafka_public_ip=$(terraform output "kafka_$($i)_public_ip").Trim('"')
            $kafka_sg_id=$(terraform output "kafka_$($i)_sg_id").Trim('"')

            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_$($i)_id" -Value $kafka_id
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_$($i)_private_ip" -Value $kafka_private_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_$($i)_public_ip" -Value $kafka_public_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.node_$($i)_sg_id" -Value $kafka_sg_id

            # Save kafka instance ip to config file
            $configOnprem.kafka | add-member -Name "node_$($i)_ip" -Value $kafka_private_ip -MemberType NoteProperty -Force
            $configOnprem.kafka | add-member -Name "node_$($i)_port" -Value "9092" -MemberType NoteProperty -Force
            $configOnprem.kafka | add-member -Name "node_$($i)_username" -Value (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.kafka.username") -MemberType NoteProperty -Force
            $configOnprem.kafka | add-member -Name "node_$($i)_private_key_path" -Value "config/$(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.kafka.keypair_name").pem" -MemberType NoteProperty -Force
        }

        # Remove temporary terraform files
        Remove-Item .terraform -Recurse -ErrorAction Ignore
        Set-Location "$($path)/.."
        # Save resources and config files
        Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        ConvertTo-Json $configOnprem | Set-Content -Path $ConfigPath

        $status = "CREATION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed creating kafka virtual machines. *****`n"
        Log-Info "Completed creating kafka virtual machines"
    } else {
        $status = "CREATION_SKIPPED"
        Write-Host "Kafka virtual machines already created."
        Log-Info "Kafka virtual machines already created"
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "kafka.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "kafka.duration" -Value $totalTime
    }
    Write-Host "Kafka virtual machines $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "Kafka virtual machines $status in $totalTime (HH:mm:ss.ms)"
}
#####################################################################################

############################# Management Station ######################################
if (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.create") {
    try{
        $status = "CREATION_STARTED"
        $startTime = $(get-date)
        if (!(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.ssh_cmd")) {
            # Notify user about start of the task
            Write-Host "`n***** Started creating management station. *****`n"
            Log-Info "Started creating management station"
        
            # Prepare terraform files
            $mgmtTerraformPath = "$path/../temp/mgmt_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
            if (Test-Path $mgmtTerraformPath) {
                Remove-Item $mgmtTerraformPath -Recurse -Force
            } 
            $null = New-Item $mgmtTerraformPath -ItemType "directory"
        
        
            # Select cloud terraform templates
            switch (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") {
                "aws" {
                    $hw_aws_cred_file = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.credentials_file"
                    $hw_aws_profile = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.profile"
                    $hw_aws_region = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.region"
                    $hw_aws_vpc = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.vpc"
                    $env_nameprefix = Get-EnvMapValue -Map $config -Key "$HWEnvironmentPrefix.prefix"
                    $hw_mgmt_instance_ami = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.instance_ami"
                    $hw_mgmt_instance_type = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.instance_type"
                    $hw_mgmt_keypair_name = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.keypair_name"
                    $hw_mgmt_ssh_allowed_cidr_blocks = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.ssh_allowed_cidr_blocks"
                    $hw_mgmt_subnet_id = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.subnet_id"
                    $hw_aws_security_group = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.aws.security_group"
                    $hw_mgmt_volume_size = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.volume_size"

                    $templateParams = @{ 
                        hw_aws_cred_file=$hw_aws_cred_file; 
                        hw_aws_profile=$hw_aws_profile; 
                        hw_aws_region=$hw_aws_region; 
                        hw_aws_vpc=$hw_aws_vpc; 
                        env_nameprefix=$env_nameprefix; 
                        hw_mgmt_instance_ami=$hw_mgmt_instance_ami; 
                        hw_mgmt_instance_type=$hw_mgmt_instance_type; 
                        hw_mgmt_keypair_name=$hw_mgmt_keypair_name;
                        hw_mgmt_ssh_allowed_cidr_blocks=$hw_mgmt_ssh_allowed_cidr_blocks
                        hw_mgmt_subnet_id=$hw_mgmt_subnet_id; 
                        hw_aws_security_group=$hw_aws_security_group; 
                        hw_mgmt_volume_size=$hw_mgmt_volume_size
                    }

                    Build-EnvTemplate -InputPath "$($path)/templates/aws/provider.tf" -OutputPath "$mgmtTerraformPath/provider.tf" -Params1 $templateParams
                    Build-EnvTemplate -InputPath "$($path)/templates/aws/mgmt.tf" -OutputPath "$mgmtTerraformPath/mgmt.tf" -Params1 $templateParams
                }    
                DEFAULT {
                    Write-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                    Log-Error "Cloud $(Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.cloud") isn't supported."
                }
            }
        
            # Initialize terraform
            Set-Location $mgmtTerraformPath
            Write-Host "Initializing terraform..."
            Log-Info "Initializing terraform"
            terraform init
            # Check for error
            if ($LastExitCode -ne 0) {
                Set-Location "$($path)/.."
                Write-Error "Can't initialize terraform. Watch logs above or check $mgmtTerraformPath folder content."
                Log-Error "Can't initialize terraform. Watch logs above or check $mgmtTerraformPath folder content."
            }
        
            # Plan cloud resources creation
            Write-Host "Executing terraform plan command..."
            Log-Info "Executing terraform plan command"
            terraform plan
            # Check for error
            if ($LastExitCode -ne 0) {
                Set-Location "$($path)/.."
                Write-Error "Can't execute terraform plan. Watch logs above or check $mgmtTerraformPath folder content."
                Log-Error "Can't execute terraform plan. Watch logs above or check $mgmtTerraformPath folder content."
            }
        
            # Create cloud resources
            Write-Host "Creating cloud resources by terraform..."
            Log-Info "Creating cloud resources by terraform"
            terraform apply -auto-approve
            # Check for error
            if ($LastExitCode -ne 0) {
                Set-Location "$($path)/.."
                Write-Error "Can't create cloud resources. Watch logs above or check $mgmtTerraformPath folder content."
                Log-Error "Can't create cloud resources. Watch logs above or check $mgmtTerraformPath folder content."
            }
        
            # Get public and private ip
            $mgmt_id=$(terraform output mgmt_id).Trim('"') 
            $mgmt_private_ip=$(terraform output mgmt_private_ip).Trim('"')
            $mgmt_public_ip=$(terraform output mgmt_public_ip).Trim('"')
            $mgmt_sg_id=$(terraform output mgmt_sg_id).Trim('"')
            # $mgmt_subnet_id=$(terraform output mgmt_subnet_id).Trim() -replace "`n" -replace "`""
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.id" -Value $mgmt_id
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.private_ip" -Value $mgmt_private_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.public_ip" -Value $mgmt_public_ip
            Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.sg_id" -Value $mgmt_sg_id
            # Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.subnet_id" -Value $mgmt_subnet_id
            Set-Location $mgmtTerraformPath
            Remove-Item .terraform -Recurse -ErrorAction Ignore
            Set-Location "$($path)/.."
            Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
        
            # Wait for vm to boot
            Write-Host "Waiting 2 minutes for mgmt station to boot..."
            Log-Info "Waiting 2 minutes for mgmt station to boot"
            Start-Sleep -Seconds 120
        
            # Copy whole project to mgmt station
            # Define os type by full path
            if (Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.copy_project_to_mgmt_station") {
                $keypairName = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.keypair_name"
                $mgmtUser = Get-EnvMapValue -Map $config -Key "$HWConfigPrefix.mgmt.username"
                $mgmtIp = Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.public_ip"
                
                Copy-Item -Path "$keypairTerraformPath/secrets/$keypairName.pem" -Destination "$path/../config/" -Recurse
                # Duplicate project folder and empty temp before running scp
                Copy-Item -Path "$path/../" -Destination "$path/../../temp/env-onprem-ps/" -Recurse
                Get-ChildItem "$path/../../temp/env-onprem-ps/temp/*" | Remove-Item -Force -Recurse 
        
                Write-Host "Copying environment management project to mgmt station..."
                Log-Info "Copying environment management project to mgmt station"

                if($IsMacOS) {
                    # macos
                    ssh -o StrictHostKeyChecking=accept-new "$($mgmtUser)@$($mgmtIp)" -i "$keypairTerraformPath/secrets/$($keypairName).pem" `
                        "mkdir -p /home/$($mgmtUser)"
                    Set-Location "$($path)/.."
                    $tmp = (Get-Item -Path ".\").FullName
                    scp -i "$keypairTerraformPath/secrets/$($keypairName).pem" -r $tmp "$($mgmtUser)@$($mgmtIp):/home/$($mgmtUser)"
                    $tmp = "$($tmp)/config"
                    scp -i "$keypairTerraformPath/secrets/$($keypairName).pem" -r $tmp "$($mgmtUser)@$($mgmtIp):/home/$($mgmtUser)"
                }
                else {
                    if ($path[0] -eq "/") {
                        # ubuntu
                        ssh -o StrictHostKeyChecking=accept-new "$($mgmtUser)@$($mgmtIp)" -i "$keypairTerraformPath/secrets/$($keypairName).pem" `
                            "mkdir -p /home/$($mgmtUser)"
                        scp -i "$keypairTerraformPath/secrets/$($keypairName).pem" -r "$path/.." `
                            "$($mgmtUser)@$($mgmtIp):/home/$($mgmtUser)"
                    } else {
                        # windows
                        ssh -o StrictHostKeyChecking=accept-new "$($mgmtUser)@$($mgmtIp)" -i "$keypairTerraformPath/secrets/$($keypairName).pem" `
                            "mkdir -p /home/$($mgmtUser)/env-onprem-ps"
                        scp -i "$keypairTerraformPath/secrets/$($keypairName).pem" -r "$path/../../temp/env-onprem-ps/*" `
                            "$($mgmtUser)@$($mgmtIp):/home/$($mgmtUser)/env-onprem-ps"
                    }
                }
                # Cleanup copy of project in temp
                Remove-Item -LiteralPath "$path/../../temp/" -Force -Recurse
                Write-Host "Done copying."
                Log-Info "Done copying"
        
                # Check for error
                if ($LastExitCode -ne 0) {
                    Write-Error "Can't copy project to mgmt station. Read logs above..."
                    Log-Error "Can't copy project to mgmt station. Read logs above..."
                }
        
                Set-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.ssh_cmd" -Value "ssh $($mgmtUser)@$($mgmtIp) -i $keypairTerraformPath/secrets/$($keypairName).pem"
                Write-EnvResources -ResourcePath $HWResourcePath -Resources $resources
                Write-Host "To continue environment creation on mgmt station use commands:"
                $sshcmd=(Get-EnvMapValue -Map $resources -Key "$HWResourcePrefix.mgmt.ssh_cmd")
                Write-Host $sshcmd
                Write-Host "cd ~/env-onprem-ps" 
        
                # Set permissions to protected key
                ssh "$($mgmtUser)@$($mgmtIp)" -i "$keypairTerraformPath/secrets/$($keypairName).pem" `
                    "chmod 600 /home/$($mgmtUser)/env-onprem-ps/config/$($keypairName).pem"
                # Set permissions to scripts
                ssh "$($mgmtUser)@$($mgmtIp)" -i "$keypairTerraformPath/secrets/$($keypairName).pem" `
                    "chmod +x /home/$($mgmtUser)/env-onprem-ps/*.sh /home/$($mgmtUser)/env-onprem-ps/*.ps1"
            }
            
            $status = "CREATION_COMPLETED"
            # Notify user about end of the task
            Write-Host "`n***** Completed creating management station. *****`n"
            Log-Info "Completed creating management station"
        } else {
            $status = "CREATION_SKIPPED"
            Write-Host "Management station virtual machine already created."
            Log-Info "Management station virtual machine already created"
        }
    }
    finally {
        if ($status -eq "CREATION_STARTED"){
            $status = "CREATION_FAILED"
        }
        $elapsedTime = $(get-date) - $startTime
        $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
        if($null -ne $statusMap){
            Set-EnvMapValue -Map $statusMap -Key "mgmt.status" -Value $status
            Set-EnvMapValue -Map $statusMap -Key "mgmt.duration" -Value $totalTime
        }
        Write-Host "Management station virtual machine $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
        Log-Info "Management station virtual machine $status in $totalTime (HH:mm:ss.ms)"
    }
}
###################################################################
