# Configure AWS instance
resource "aws_instance" "timescale_<%=node_index%>" {
  ami                         = "<%=hw_timescale_instance_ami%>"
  instance_type               = "<%=hw_timescale_instance_type%>"
  key_name                    = "<%=hw_timescale_keypair_name%>"
  associate_public_ip_address = true
  subnet_id                   = "<%=hw_timescale_subnet_id%>"
  vpc_security_group_ids      = [aws_security_group.timescale_<%=node_index%>.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = "<%=hw_timescale_volume_size%>"
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "<%=env_nameprefix%>_timescale_<%=node_index%>"
  }
}

# Configure AWS security group
resource "aws_security_group" "timescale_<%=node_index%>" {
  name        = "<%=env_nameprefix%>_timescale_<%=node_index%>"
  description = "timescale access"
  vpc_id      = "<%=hw_aws_vpc%>"

  ingress {
    description = "access_from_mgmt"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_mgmt_subnet_cidr%>"]
  }

  // ingress {
  //   description = "access_from_k8s"
  //   from_port   = 5432
  //   to_port     = 5432
  //   protocol    = "tcp"
  //   cidr_blocks = ["<%=hw_k8s_subnet_cidr%>"]
  // }

  ingress {
    description = "access_beetween_timescale_nodes"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_timescale_subnet_cidr%>"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "<%=env_nameprefix%>_timescale_<%=node_index%>_sg"
  }
}

# Save timescale instance id
output "timescale_<%=node_index%>_id" {
  value       = aws_instance.timescale_<%=node_index%>.id
  description = "The unique identifier of the timescale virtual machine."
}

# Save timescale security group id
output "timescale_<%=node_index%>_sg_id" {
  value       = aws_security_group.timescale_<%=node_index%>.id
  description = "The unique identifier of the timescale security group."
}

# Save timescale instance private ip
output "timescale_<%=node_index%>_private_ip" {
  value       = aws_instance.timescale_<%=node_index%>.private_ip
  description = "The private IP address of the timescale virtual machine."
}

# Save timescale instance public ip
output "timescale_<%=node_index%>_public_ip" {
  value       = aws_instance.timescale_<%=node_index%>.public_ip
  description = "The public IP address of the timescale virtual machine."
}
