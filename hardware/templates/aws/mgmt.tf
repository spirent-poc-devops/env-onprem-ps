# Configure AWS instance
resource "aws_instance" "mgmt" {
  ami                         = "<%=hw_mgmt_instance_ami%>"
  instance_type               = "<%=hw_mgmt_instance_type%>"
  key_name                    = "<%=hw_mgmt_keypair_name%>"
  associate_public_ip_address = true
  subnet_id                   = "<%=hw_mgmt_subnet_id%>"
  vpc_security_group_ids      = [aws_security_group.mgmt.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = "<%=hw_mgmt_volume_size%>"
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "<%=env_nameprefix%>_mgmt"
  }
}

# Configure AWS security group
resource "aws_security_group" "mgmt" {
  name        = "<%=env_nameprefix%>_mgmt"
  description = "Management station access"
  vpc_id      = "<%=hw_aws_vpc%>"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_mgmt_ssh_allowed_cidr_blocks%>"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "<%=env_nameprefix%>_mgmt_sg"
  }
}

# Save mgmt instance id
output "mgmt_id" {
  value       = aws_instance.mgmt.id
  description = "The unique identifier of the management station virtual machine."
}

# Save mgmt security group id
output "mgmt_sg_id" {
  value       = aws_security_group.mgmt.id
  description = "The unique identifier of the management station security group."
}

# Save mgmt instance private ip
output "mgmt_private_ip" {
  value       = aws_instance.mgmt.private_ip
  description = "The private IP address of the management station virtual machine."
}

# Save mgmt instance public ip
output "mgmt_public_ip" {
  value       = aws_instance.mgmt.public_ip
  description = "The public IP address of the management station virtual machine."
}
