module "ssh_key_pair" {
  source = "cloudposse/key-pair/aws"
  # Cloud Posse recommends pinning every module to a specific version
  # version     = "x.x.x"
  namespace             = "<%=hw_aws_namespace%>"
  stage                 = "<%=hw_aws_stage%>"
  name                  = "<%=env_nameprefix%>"
  ssh_public_key_path   = "./secrets"
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
}

output "keypair_name" {
  value       = module.ssh_key_pair.key_name
  description = "The keypair name"
}