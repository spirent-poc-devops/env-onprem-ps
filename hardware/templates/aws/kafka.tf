# Configure AWS instance
resource "aws_instance" "kafka_<%=node_index%>" {
  ami                         = "<%=hw_kafka_instance_ami%>"
  instance_type               = "<%=hw_kafka_instance_type%>"
  key_name                    = "<%=hw_kafka_keypair_name%>"
  associate_public_ip_address = true
  subnet_id                   = "<%=hw_kafka_subnet_id%>"
  vpc_security_group_ids      = [aws_security_group.kafka_<%=node_index%>.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = "<%=hw_kafka_volume_size%>"
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "<%=env_nameprefix%>_kafka_<%=node_index%>"
  }
}

# Configure AWS security group
resource "aws_security_group" "kafka_<%=node_index%>" {
  name        = "<%=env_nameprefix%>_kafka_<%=node_index%>"
  description = "kafka access"
  vpc_id      = "<%=hw_aws_vpc%>"

  ingress {
    description = "access_from_mgmt_and_other_kafka_nodes"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_mgmt_subnet_cidr%>", "<%=hw_kafka_subnet_cidr%>"]
  }
  
  ingress {
    description = "access_to_kafka"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "access_to_zookeeper"
    from_port   = 2181
    to_port     = 2181
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "<%=env_nameprefix%>_kafka_<%=node_index%>_sg"
  }
}

# Save kafka instance id
output "kafka_<%=node_index%>_id" {
  value       = aws_instance.kafka_<%=node_index%>.id
  description = "The unique identifier of the kafka virtual machine."
}

# Save kafka security group id
output "kafka_<%=node_index%>_sg_id" {
  value       = aws_security_group.kafka_<%=node_index%>.id
  description = "The unique identifier of the kafka security group."
}

# Save kafka instance private ip
output "kafka_<%=node_index%>_private_ip" {
  value       = aws_instance.kafka_<%=node_index%>.private_ip
  description = "The private IP address of the kafka virtual machine."
}

# Save kafka instance public ip
output "kafka_<%=node_index%>_public_ip" {
  value       = aws_instance.kafka_<%=node_index%>.public_ip
  description = "The public IP address of the kafka virtual machine."
}
