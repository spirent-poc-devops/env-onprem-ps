# Configure AWS instance
resource "aws_instance" "k8s_<%=hw_k8s_vm_index%>" {
  ami                         = "<%=hw_k8s_instance_ami%>"
  instance_type               = "<%=hw_k8s_instance_type%>"
  key_name                    = "<%=hw_k8s_keypair_name%>"
  associate_public_ip_address = true
  subnet_id                   = "<%=hw_k8s_subnet_id%>"
  security_groups               = ["<%=hw_aws_security_group%>"]
  vpc_security_group_ids      = [aws_security_group.k8s_<%=hw_k8s_vm_index%>.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = "<%=hw_k8s_volume_size%>"
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "k8s_<%=hw_k8s_vm_index%>_<%=env_nameprefix%>"
  }
}

# Configure AWS security group
resource "aws_security_group" "k8s_<%=hw_k8s_vm_index%>" {
  name        = "k8s_<%=hw_k8s_vm_index%>_<%=env_nameprefix%>"
  description = "k8s_<%=hw_k8s_vm_index%> access"
  vpc_id      = "<%=hw_aws_vpc%>"

  ingress {
    description = "access_from_mgmt"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_mgmt_subnet_cidr%>"]
  }

  ingress {
    description = "access_bw_k8s_nodes"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_k8s_subnet_cidr%>"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "k8s_<%=hw_k8s_vm_index%>_<%=env_nameprefix%>_sg"
  }
}

# Save k8s instance id
output "k8s_<%=hw_k8s_vm_index%>_id" {
  value       = aws_instance.k8s_<%=hw_k8s_vm_index%>.id
  description = "The unique identifier of the k8s virtual machine <%=hw_k8s_vm_index%>."
}

# Save k8s security group id
output "k8s_<%=hw_k8s_vm_index%>_sg_id" {
  value       = aws_security_group.k8s_<%=hw_k8s_vm_index%>.id
  description = "The unique identifier of the k8s security group <%=hw_k8s_vm_index%>."
}

# Save k8s instance private ip
output "k8s_<%=hw_k8s_vm_index%>_private_ip" {
  value       = aws_instance.k8s_<%=hw_k8s_vm_index%>.private_ip
  description = "The private IP address of the k8s virtual machine <%=hw_k8s_vm_index%>."
}

# Save k8s instance public ip
output "k8s_<%=hw_k8s_vm_index%>_public_ip" {
  value       = aws_instance.k8s_<%=hw_k8s_vm_index%>.public_ip
  description = "The public IP address of the k8s virtual machine <%=hw_k8s_vm_index%>."
}
