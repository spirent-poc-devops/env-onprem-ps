# Configure AWS instance
resource "aws_instance" "k8s_lb" {
  ami                         = "<%=hw_k8s_lb_instance_ami%>"
  instance_type               = "<%=hw_k8s_lb_instance_type%>"
  key_name                    = "<%=hw_k8s_lb_keypair_name%>"
  associate_public_ip_address = true
  subnet_id                   = "<%=hw_k8s_lb_subnet_id%>"
  security_groups               = ["<%=hw_aws_security_group%>"]
  vpc_security_group_ids      = [aws_security_group.k8s_lb.id]

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "k8s_lb_<%=env_nameprefix%>"
  }
}

# Configure AWS security group
resource "aws_security_group" "k8s_lb" {
  name        = "k8s_lb_<%=env_nameprefix%>"
  description = "k8s_lb access"
  vpc_id      = "<%=hw_aws_vpc%>"

    ingress {
    description = "access_from_mgmt"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_mgmt_subnet_cidr%>"]
  }

  ingress {
    description = "access_from_k8s_nodes"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_k8s_subnet_cidr%>"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "k8s_lb_<%=env_nameprefix%>_sg"
  }
}

# Save k8s_lb instance id
output "k8s_lb_id" {
  value       = aws_instance.k8s_lb.id
  description = "The unique identifier of the k8s loadbalancer virtual machine."
}

# Save k8s_lb security group id
output "k8s_lb_sg_id" {
  value       = aws_security_group.k8s_lb.id
  description = "The unique identifier of the k8s loadbalancer security group."
}

# Save k8s_lb instance private ip
output "k8s_lb_private_ip" {
  value       = aws_instance.k8s_lb.private_ip
  description = "The private IP address of the k8s loadbalancer virtual machine."
}

# Save k8s_lb instance public ip
output "k8s_lb_public_ip" {
  value       = aws_instance.k8s_lb.public_ip
  description = "The public IP address of the k8s loadbalancer virtual machine."
}
