# Configure AWS instance
resource "aws_instance" "neo4j_<%=node_index%>" {
  ami                         = "<%=hw_neo4j_instance_ami%>"
  instance_type               = "<%=hw_neo4j_instance_type%>"
  key_name                    = "<%=hw_neo4j_keypair_name%>"
  associate_public_ip_address = true
  subnet_id                   = "<%=hw_neo4j_subnet_id%>"
  vpc_security_group_ids      = [aws_security_group.neo4j_<%=node_index%>.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = "<%=hw_neo4j_volume_size%>"
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "<%=env_nameprefix%>_neo4j_<%=node_index%>"
  }
}

# Configure AWS security group
resource "aws_security_group" "neo4j_<%=node_index%>" {
  name        = "<%=env_nameprefix%>_neo4j_<%=node_index%>"
  description = "neo4j access"
  vpc_id      = "<%=hw_aws_vpc%>"

  ingress {
    description = "access_from_mgmt_and_other_neo4j_nodes"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["<%=hw_mgmt_subnet_cidr%>", "<%=hw_neo4j_subnet_cidr%>"]
  }

  ingress {
    description = "access_to_neo4j"
    from_port   = 7474
    to_port     = 7474
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress { 
    description = "access_to_neo4j_bolt"
    from_port   = 7687
    to_port     = 7687
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "<%=env_nameprefix%>"
    Name        = "<%=env_nameprefix%>_neo4j_<%=node_index%>_sg"
  }
}

# Save neo4j instance id
output "neo4j_<%=node_index%>_id" {
  value       = aws_instance.neo4j_<%=node_index%>.id
  description = "The unique identifier of the neo4j virtual machine."
}

# Save neo4j security group id
output "neo4j_<%=node_index%>_sg_id" {
  value       = aws_security_group.neo4j_<%=node_index%>.id
  description = "The unique identifier of the neo4j security group."
}

# Save neo4j instance private ip
output "neo4j_<%=node_index%>_private_ip" {
  value       = aws_instance.neo4j_<%=node_index%>.private_ip
  description = "The private IP address of the neo4j virtual machine."
}

# Save neo4j instance public ip
output "neo4j_<%=node_index%>_public_ip" {
  value       = aws_instance.neo4j_<%=node_index%>.public_ip
  description = "The public IP address of the neo4j virtual machine."
}
