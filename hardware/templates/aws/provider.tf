# Configure the AWS Provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region                  = "<%=hw_aws_region%>"
  shared_credentials_file = "<%=hw_aws_cred_file%>"
  profile                 = "<%=hw_aws_profile%>"
}