function Run-Command 
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,Position=0)]
        [string] $Command
    )
    if (Invoke-Expression $Command)
    { 
        Write-Output "Success"
    }else
    { 
        Write-Output "Fail"
    }
}

function Match-CommandOutput 
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,Position=0)]
        [AllowEmptyString()]
        [string] $Output,

        [Parameter(Mandatory=$true,Position=1)]
        [string] $Expression
    )
    # Sanity check
    if(($Output -eq $null) -or ($Output -eq ""))
    {
        Write-Output "Fail"
    }
    # Try to match the command's output
    elseif ($Output -match $Expression)
    { 
        Write-Output "Success"
    } 
    else
    { 
        Write-Output "Fail"
    }
}

#Check-Pods  - Check the pods
function Check-Pods 
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,Position=0)]
        [string] $Pod,

        [Parameter(Mandatory=$true,Position=1)]
        [string] $namespace 
    )
    $Command='kubectl get po $Pod -n $Namespace'
    Write-Output (Run-Command $Command)
}


#Check-Namespace  - Check the namespaces
function Check-Namespace 
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,Position=0)]
        [string] $Namespace
    )
    $Command='kubectl get ns $Namespace'
    Write-Output (Run-Command $Command)
}

#Print-TestComponent  - Print the report component
function Print-TestComponent
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true,Position=0)]
        [string] $Component,
        [Parameter(Mandatory=$true, Position=1)]
        [hashtable] $Report
    )

    Write-Host $('**************** Test Report for component {0} *********************' -f $Component)
    foreach($key in $Report.keys)
    {
        $message = '{0}  -  {1} ' -f $key, $Report[$key]
        if ($Report[$key] -eq "Success"){
            Write-Host $message -ForegroundColor Green
        }else{
            Write-Host $message -ForegroundColor Red
        }
        
    }
    Write-Host $('**************** End of Test Report for component {0} *********************' -f $Component)
}


#Print-TestReport  - Print the test report
function Print-TestReport
{
    [CmdletBinding()]
    param (
    [Parameter(Mandatory=$true, Position=1)]
    [hashtable] $Report
    )

    Write-Host "**************** Test Report ********************* "
    foreach($key in $Report.keys)
    {
        $message = '{0}  -  {1} ' -f $key, $Report[$key]
        Write-Host $message
    }
    Write-Host "**************** End of Test Report ********************* "
} 


