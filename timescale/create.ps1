#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "timescale",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)

#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath


###################################################################
try{
    $status = "CREATION_STARTED"
    $startTime = $(get-date)
    # Skip if already installed
    if (!(Get-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint")) {
        # Notify user about start of the task
        Write-Host "`n***** Started creating timescale database. *****`n"
        Log-Info "Started creating timescale database"

        # Check timescaleql version
        if ((Get-EnvMapValue -Map $config -Key "timescale.timescale_version") -lt 11) {
            Write-Error "Use timescale version 11 or higher to install timescale db."
            Log-Error "Use timescale version 11 or higher to install timescale db."
        }

        # Prepare hosts file
        $ansible_inventory = @("[master]")
        $ansible_inventory += "0 ansible_host=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_private_key_path")"
        $ansible_inventory += "`n[standby]"
        $timescaleVMCount = $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.nodes_count")
        for ($i = 1; $i -lt $timescaleVMCount; $i++) {
            $ansible_inventory += "$i ansible_host=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_private_key_path")"
        }
        Set-Content -Path "$path/../temp/timescale_ansible_hosts" -Value $ansible_inventory

        # Whitelist nodes
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/timescale_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/timescale_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        }

        # Install timescale database
        $timescale_db_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.timescale_version"
        $timescale_name = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.name"
        $timescale_username = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username"
        $timescale_password = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.password"
        $hw_timescale_master_ip = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_ip"
        $timescale_standby_replications_list = ""
        for ($i = 1; $i -lt $timescaleVMCount; $i++) {
            $timescale_standby_replications_list += "host replication repuser $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip")/32 trust"
        }

        $templateParams = @{ 
            timescale_db_version=$timescale_db_version; 
            timescale_name=$timescale_name; 
            timescale_username=$timescale_username; 
            timescale_password=$timescale_password;
            timescale_standby_replications_list=$timescale_standby_replications_list
            hw_timescale_master_ip=$hw_timescale_master_ip
            }

        Build-EnvTemplate -InputPath "$($path)/templates/timescale_install_playbook.yml" -OutputPath "$($path)/../temp/timescale_install_playbook.yml" -Params1 $templateParams
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/timescale_ansible_hosts" "$path/../temp/timescale_install_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/timescale_ansible_hosts" "$path/../temp/timescale_install_playbook.yml"
        }

        # Write timescale resources
        $port=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_port")
        $ip=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_0_ip")
        $endpoint = "$($ip):$($port)"
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.host" -Value $ip
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.port" -Value $port
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint" -Value $endpoint
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.inventory" -Value $ansible_inventory
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
        $status = "CREATION_COMPLETED"
        # Notify user about end of the task
        Write-Host "`n***** Completed creating timescale database. *****`n"
        Log-Info "Completed creating timescale database"
    } else
    {
        $status = "CREATION_SKIPPED"
        Write-Host "timescale component already installed. Installation skipped."
        Log-Info "timescale component already installed. Installation skipped."
    }
}
finally {
    if ($status -eq "CREATION_STARTED"){
        $status = "CREATION_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################
