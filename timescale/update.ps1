#!/usr/bin/env pwsh

param
(
    [Alias("c", "Config")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $ConfigPrefix = "timescale",

    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $ResourcePrefix
)
#Set Log Component 
$env:LogComponent=$ConfigPrefix

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/../common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}
if (($ResourcePrefix -eq $null) -or ($ResourcePrefix -eq "")) 
{ 
    $ResourcePrefix = $ConfigPrefix 
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath


###################################################################
try{
    $status = "UPDATE_STARTED"
    $startTime = $(get-date)
    # Skip if already installed
    # if (!(Get-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint")) {
    if ((Get-EnvMapValue -Map $config -Key "$ConfigPrefix.update") -eq "true"){
        # Notify user about start of the task
        Write-Host "`n***** Started updating timescale database. *****`n"
        Log-Info "Started updating timescale database"

        # Check timescaleql version
        if ((Get-EnvMapValue -Map $config -Key "timescale.timescale_version") -lt 11) {
            Write-Error "Use timescale version 11 or higher to install timescale db."
            Log-Error "Use timescale version 11 or higher to install timescale db."
        }

        # Prepare hosts file
        $ansible_inventory = @("[timescale]")
        $timescaleVMCount = $(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.nodes_count")
        for ($i = 0; $i -lt $timescaleVMCount; $i++) {
            $ansible_inventory += "$i ansible_host=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.node_$i`_private_key_path")"
        }
        Set-Content -Path "$path/../temp/timescale_ansible_hosts" -Value $ansible_inventory

        # Whitelist nodes
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/timescale_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/timescale_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
        }

        # Install timescale database
        $timescale_db_timescale_version = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.timescale_version"
        $timescale_name = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.name"
        $timescale_username = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.username"
        $timescale_password = Get-EnvMapValue -Map $config -Key "$ConfigPrefix.password"
        $templateParams = @{ timescale_db_timescale_version=$timescale_db_timescale_version ; timescale_name=$timescale_name ; timescale_username=$timescale_username ; timescale_password=$timescale_password }
        Build-EnvTemplate -InputPath "$($path)/templates/timescale_install_playbook.yml" -OutputPath "$($path)/../temp/timescale_install_playbook.yml" -Params1 $templateParams
        if ($config["extended_ansible_logs"]) {
            ansible-playbook -v -i "$path/../temp/timescale_ansible_hosts" "$path/../temp/timescale_install_playbook.yml"
        } else {
            ansible-playbook -i "$path/../temp/timescale_ansible_hosts" "$path/../temp/timescale_install_playbook.yml"
        }

        # Write timescale resources
        $port=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.port")
        $ip=(Get-EnvMapValue -Map $config -Key "$ConfigPrefix.ip")
        $endpoint = "$($ip):$($port)"
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.host" -Value $ip
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.port" -Value $port
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.endpoint" -Value $endpoint
        Set-EnvMapValue -Map $resources -Key "$ResourcePrefix.inventory" -Value $ansible_inventory
        Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
        $status = "UPDATE_COMPLETED"

        Set-EnvMapValue -Map $config -Key "$ConfigPrefix.update" -Value false
        Write-EnvResources -ResourcePath $ConfigPath -Resources $config
        
        # Notify user about end of the task
        Write-Host "`n***** Completed updating timescale database. *****`n"
        Log-Info "Completed updating timescale database"
    # } else
    # {
    #     $status = "UPDATE_SKIPPED"
    #     Write-Host "timescale component already installed. Installation skipped."
    #     Log-Info "timescale component already installed. Installation skipped."
    # }
        }   
        else
        {
        $status = "UPDATE_SKIPPED"
        Write-Host "Update flag not set. So, skipping update for $ConfigPrefix" -ForegroundColor Yellow
        Log-Error "Update flag not set. So, skipping update for $ConfigPrefix"
        }   
    }
finally {
    if ($status -eq "UPDATE_STARTED"){
        $status = "UPDATE_FAILED"
    }
    $elapsedTime = $(get-date) - $startTime
    $totalTime = "{0:HH:mm:ss.ffff}" -f ([datetime]$elapsedTime.Ticks)
    if($null -ne $statusMap){
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.status" -Value $status
        Set-EnvMapValue -Map $statusMap -Key "$ResourcePrefix.duration" -Value $totalTime
    }
    Write-Host "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)" -ForegroundColor Yellow
    Log-Info "$ResourcePrefix $status in $totalTime (HH:mm:ss.ms)"
}
###################################################################

